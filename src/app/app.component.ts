import { Component, OnInit, HostListener, ViewChild, ElementRef, NgZone} from '@angular/core';
import { fade } from './animations/animations';
import * as Tone from 'tone';
import { FormControl, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { of, from, fromEvent, Subject, interval, timer } from 'rxjs';
import { mergeMap, distinct, toArray, pluck, tap, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { SimpleOption } from './types/Types';
import { Bind } from './decorators/shrared.decorators';

@Component({
  selector: 'ui-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fade]
})
export class AppComponent implements OnInit {
  isShowContent = false;
  // selectOptions: SimpleOption<number>[] = [
  //   {title: 'One', value: 1},
  //   {title: 'Two', value: 2},
  //   {title: 'Three', value: 3},
  //   {title: 'Four', value: 4},
  // ]

  datepickerFrom: FormControl = new FormControl(new Date, Validators.required);
  datepickerTo: FormControl = new FormControl(new Date, null);

  // @ViewChild('phoneRef', { read: PhoneComponent }) phoneRef: PhoneComponent;
  
  @ViewChild('btn', { static: false }) btnRef: ElementRef;
  @ViewChild('smileRef', { static: false }) smileRef: ElementRef;
  @ViewChild('emojiRef') emojiRef: ElementRef;
  @ViewChild('smartRef') smartRef: ElementRef;

  @HostListener('window:keyup.control.enter', ["$event"])
    check(e: KeyboardEvent) {
      // console.log('Event', e)
    }

  constructor( ) {
  }

  ngOnInit() {}


  showDates() {
    console.log('From:', this.datepickerFrom.value);
    console.log('To:', this.datepickerTo.value);
  }

  showContent() {
    this.isShowContent = true;

    // let timeout = setTimeout(_ => {
    //   this.bottom();
    //   clearTimeout(timeout);
    //   timeout = null;
    // }, 0);
  }

  bottom() {
    const { scrollHeight, clientHeight } = document.documentElement;
    const offset: number = scrollHeight - clientHeight;
    window.scrollTo({
      top: offset,
      behavior: 'smooth'
    })

  }
}

// const unsub$: Subject<void> = new Subject();


// const nums: number[] = [1, 2, 3, 4, 5, 5, 6, 5];

// from(nums)
// .pipe(distinctUntilChanged())
// .subscribe(v => {
//   console.log(v)
// })

// interval(1000)
// .pipe(takeUntil(unsub$))
// .subscribe(v => {
//   console.log(v)
// })
// interval(1000)
// .pipe(takeUntil(timer(5000)))
// .subscribe(v => {
//   console.log(v)
// })

// fromEvent(document.body, 'click')
// .pipe(pluck('target'))
// .subscribe(e => console.log(e))
// fromEvent(document.body, 'click')
// .pipe(
//   pluck('target')
// )
// .subscribe((el$: HTMLElement) => {
//   console.log(el$)
// })
// const users: any[] = [
//   {id: 1, name: 'John'},
//   {id: 2, name: 'Harold'},
//   {id: 3, name: 'Henry'},
//   {id: 1, name: 'John'},
//   {id: 2, name: 'Harold'},
// ]

// of(users).pipe(
//   mergeMap(users => {
//     return from(users);
//   }),
//   distinct(u => u.name),
//   toArray()
// ).subscribe(usrs => console.log(usrs));

// from(users).pipe(
//   distinct(u => u.id),
//   reduce((accum, curr) => accum.concat(curr), [])
// ).subscribe(unique => console.log(unique));

// function dist(arr: any[], fn: Function) {
//   const { result } = arr.reduce((accum, current) => {
//     const value: any = fn(current);

//     if(!accum.values.includes(value)) {
//       accum.values.push(value);
//       accum.result.push(current);
//     }

//     return accum;

//   }, { result: [], values: [] });


//   return result;
// }

// console.log(dist(users, (u) => u.id));
