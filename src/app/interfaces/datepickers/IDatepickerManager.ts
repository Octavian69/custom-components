import { DPAction, DateLimitType, DatePickerLevelType } from '../../types/datepicker.types';
import { DatePickerOption } from '../../models/datepicker-models/DatepcikerOption';

export interface IDatepickerManager<T> {
    init(): void;
    set(option: DatePickerOption): void;
    complete?(param?: number): void;
    change?(action: DPAction): void;
    checked?(...param: any): any
    getLimitDate(type: DateLimitType): number | Date;
    reset(stateLevel?: DatePickerLevelType): void;
    getList(): T
}