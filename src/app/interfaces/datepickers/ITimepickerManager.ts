import { TimeEvent } from '../../types/datepicker.types';
import { IDPStateManager } from './IDPStateManager';


export type TimepickerMethods = 'getShowState' | 'setShowState' | 'setShowStatus' | 'open' | 'close';

export interface ITimepickerManager extends Pick<IDPStateManager, TimepickerMethods> {
    set(event: TimeEvent): void;
    get(): [string, string];
}