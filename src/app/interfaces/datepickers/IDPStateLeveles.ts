import { DatePickerLevelType, DPListType } from '../../types/datepicker.types';
import { IDatepickerManager } from './IDatepickerManager';

export type IDPStateLeveles = Record<DatePickerLevelType, IDatepickerManager<DPListType>>

// export type IDPStateLeveles = {
//     [prop in DatePickerLevelType]: IDatepickerManager<DPCompleteParam, DPSetParam>;
// };