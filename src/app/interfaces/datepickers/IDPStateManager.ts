import { IDatepickerManager } from './IDatepickerManager';
import { DatePickerLevelType, DPListType, DateLimitType } from '../../types/datepicker.types';

export interface IDPStateManager extends Partial<IDatepickerManager<DPListType>>  {
    get: () => Date;
    next(key: DatePickerLevelType): void;
    setFullDate(date: Date): void; 
    getShowState(): boolean;
    setShowStatus(flag: boolean): void;
    setLimitDate(type: DateLimitType, date: Date): void;
    setShowState(): void;
    open(): void;
    close(): void;
    setLevel(type: DatePickerLevelType): void;
    getLevel(level: DatePickerLevelType): IDatepickerManager<DPListType>;
}