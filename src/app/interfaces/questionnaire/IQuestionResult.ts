import { TQuestionResultOption } from 'src/app/types/questionnaire.types';

export interface IQuestionResult {
    failed: TQuestionResultOption[],
    success: TQuestionResultOption[],
    efficiency: number
}