import { TQuestionControl } from 'src/app/types/questionnaire.types';

export interface IQuestion {
    type: TQuestionControl;
    formControlName: string;
    title: string;
    defaultValue: any;
    correctValue: any;
    multiple: boolean;
}