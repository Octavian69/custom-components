import { IQuestion } from './IQuestion';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

export interface IQuestionThumbler extends IQuestion {
    multiple: boolean;
    options: TQuestionOption[];
}