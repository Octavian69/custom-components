import { IQuestion } from './IQuestion';
import { TQuestionImgOption } from 'src/app/types/questionnaire.types';

export interface IQuestionImg extends IQuestion {
    options: TQuestionImgOption[];
}