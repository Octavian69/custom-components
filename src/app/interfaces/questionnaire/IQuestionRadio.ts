import { IQuestion } from './IQuestion';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

export interface IQuestionRadio extends IQuestion {
    options: TQuestionOption[]
}