import { NgModule, ModuleWithProviders } from '@angular/core';

@NgModule({
    providers: []
})
export class ServicesModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServicesModule,
            providers: []
        }
    }
}