//Modules

import { NgModule } from '@angular/core';
import { CoreModule } from './core.module';
import { MaterialModule } from './material.module';
import { PipesModule } from './pipes.module';
import { DirectivesModule } from './directives.module';

//Components

import { InputRangeComponent } from '../components/input-range/input-range.component';
import { CircleComponent } from '../components/circle/circle.component';
import { DropListComponent } from '../components/drop-list/drop-list.component';
import { RowComponent } from '../components/row/row.component';
import { InfoStepperComponent } from '../components/info-stepper/info-stepper.component';
import { InfoStepComponent } from '../components/info-step/info-step.component';
import { PercentPopupComponent } from '../components/percent-popup/percent-popup.component';
import { ProgressPageRowComponent } from '../components/progress-page-row/progress-page-row.component';
import { PhoneComponent } from '../components/phone/phone.component';
import { ClipPathComponent } from '../components/clip-path/clip-path.component';
import { ProgressTriggerComponent } from '../components/progress-trigger/progress-trigger.component';
import { RangeComponent } from '../components/range/range.component';
import { ConnectedArrowsComponent } from '../components/connected-arrows/connected-arrows.component';
import { ScrollArrowComponent } from '../components/scroll-arrow/scroll-arrow.component';
import { LayerCardComponent } from '../components/layer-card/layer-card.component';
import { CheckboxFilterComponent } from '../components/checkbox-filter/checkbox-filter.component';
import { CheckboxMultipleComponent } from '../components/checkbox-multiple/checkbox-multiple.component';
import { DatePickerComponent } from '../components/date-picker/date-picker.component';
import { DatePickerListComponent } from '../components/date-picker-list/date-picker-list.component';
import { TimepickerComponent } from '../components/timepicker/timepicker.component';
import { FormRadioComponent } from '../components/form-radio/form-radio.component';
import { QuestionnaireComponent } from '../components/questionnaire/questionnaire.component';
import { QuestionnaireStartComponent } from '../components/questionnaire-start/questionnaire-start.component';
import { QuestionnaireFormComponent } from '../components/questionnaire-form/questionnaire-form.component';
import { QuestionnaireResultComponent } from '../components/questionnaire-result/questionnaire-result.component';
import { FormThumblerComponent } from '../components/form-thumbler/form-thumbler.component';
import { FormImgComponent } from '../components/form-img/form-img.component';
import { FormRadioInputComponent } from '../components/form-radio-input/form-radio-input.component';
import { FormThumlerInputComponent } from '../components/form-thumler-input/form-thumler-input.component';
import { FormImgInputComponent } from '../components/form-img-input/form-img-input.component';
import { PreloaderComponent } from '../components/preloader/preloader.component';
import { TranslateComponent } from '../components/translate/translate.component';
import { FormTextareaComponent } from '../components/form-textarea/form-textarea.component';


const declarations: any[] = [
    InputRangeComponent,
    CircleComponent,
    DropListComponent,
    RowComponent,
    InfoStepperComponent,
    InfoStepComponent,
    PercentPopupComponent,
    ProgressPageRowComponent,
    PhoneComponent,
    ClipPathComponent,
    ProgressTriggerComponent,
    RangeComponent,
    ConnectedArrowsComponent,
    ScrollArrowComponent,
    LayerCardComponent,
    PreloaderComponent,
    CheckboxFilterComponent,
    CheckboxMultipleComponent,
    DatePickerComponent,
    DatePickerListComponent,
    TimepickerComponent,
    FormRadioComponent,
    FormRadioInputComponent,
    FormThumblerComponent,
    FormThumlerInputComponent,
    FormImgComponent,
    FormImgInputComponent,
    FormTextareaComponent,
    QuestionnaireComponent,
    QuestionnaireStartComponent,
    QuestionnaireFormComponent,
    QuestionnaireResultComponent,
    TranslateComponent
];

const imports: any[] = [
    CoreModule, 
    MaterialModule, 
    PipesModule, 
    DirectivesModule
];

@NgModule({
    declarations,
    imports,
    exports: declarations
})
export class ComponentsModule {}