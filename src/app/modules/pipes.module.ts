import { NgModule } from '@angular/core';
import { SuffixPipe } from '../pipes/suffix.pipe';

const declarations = [
    SuffixPipe
];

@NgModule({
    declarations,
    exports: declarations
})
export class PipesModule {} 