import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const imports: any[] = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
]

@NgModule({
    imports,
    exports: imports
})
export class CoreModule {}