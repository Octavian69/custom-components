import { NgModule } from '@angular/core';
import { LazyFileDirective } from '../directives/lazy-file.directive';
import { AutoScrollManagerDirective } from '../directives/auto-scroll-manager.directive';
import { BreakPageDirective } from '../directives/break-page.directive';
import { ThemeDirective } from '../directives/theme.directive';

const directives: any[] = [
    LazyFileDirective,
    AutoScrollManagerDirective,
    BreakPageDirective,
    ThemeDirective
]

@NgModule({
    declarations: directives,
    exports: directives
})
export class DirectivesModule {}