import { NgModule } from '@angular/core';
import { CoreModule } from './core.module';
import { MaterialModule } from './material.module';
import { ComponentsModule } from './components.module';
import { DirectivesModule } from './directives.module';
import { PipesModule } from './pipes.module';

const modules: any[] = [
    CoreModule,
    DirectivesModule,
    PipesModule,
    MaterialModule,
    ComponentsModule
];

@NgModule({
    imports: modules,
    exports: modules,
})
export class SharedModule {}