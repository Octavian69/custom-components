export class QuestionnaireFlags {
    public isLoadQuestions: boolean = false;
    public isSetQuestion: boolean = false;
    public isQuestionsRequest: boolean = false;

    constructor() {}
}