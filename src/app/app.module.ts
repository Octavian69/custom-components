import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import ruLocale from '@angular/common/locales/ru';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { SharedModule } from './modules/shared.module';
import { ServicesModule } from './modules/services.module';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';

registerLocaleData(ruLocale, 'ru');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServicesModule.forRoot(),
    HttpClientModule,
    PickerModule,
    EmojiModule,
    SharedModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
