export function Bind(): (...args) => PropertyDescriptor  {
    return (target: any, fnName: string, descriptor: PropertyDescriptor): PropertyDescriptor => {
        const { value } = descriptor;

        return {
            get() {
                return value.bind(this);
            }
        }
    }

}