import { TSide, NumberStatus } from './Types';

export type ANTranslateType = 'translate' | 'translateX' | 'translateY';
export type AxisType = 'X' | 'Y' | '';
export type ANTranslatOption = {
    side: TSide,
    status: NumberStatus
}