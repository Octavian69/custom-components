import { Months, dpDaysTitles } from '../db/datepicker.db';
import { DatePickerOption } from '../models/datepicker-models/DatepcikerOption';

export type DatePickerLevelType = 'year' | 'month' | 'date';
export type DatePickerStateTitle = 'Год' | 'Месяц' | 'Число';
export type DateLimitType = 'min' | 'max';
export type DPAction = 'inc' | 'dec';
export type Month = typeof Months[number];
export type DPCompleteParam = number;
export type DPSetParam = number;
export type DayTitle = typeof dpDaysTitles[number];
export type DPDateListType = DatePickerOption[][];
export type DPSimpleListType = DatePickerOption[];
export type DPListType = DPDateListType | DPSimpleListType;

export type PickerType = 'timepicker' | 'datepicker';

export type DPSectionType = {
    type: DatePickerLevelType,
    label: DatePickerStateTitle
}

export type TimeEvent = {
    type: TimeType,
    action: DPAction
};

export type DateEvent = {
    option: DatePickerOption,
    nextLevel: DatePickerLevelType
};

// export type DateNextLevels = Record<keyof DateEvent, DatePickerLevelType>

// Timepicker

export type TimeType = 'hours' | 'minutes';