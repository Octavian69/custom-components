import { TaskListType } from './Types'

export type DropListItems = {
    [key in TaskListType]: string[]
}

export type DropReplaceKey = 'todo-active' | 'active-todo';

export type DropReplaceItem = {
    from: string,
    to: string
}

export type DropReplaceItems = Record<DropReplaceKey, DropReplaceItem>
