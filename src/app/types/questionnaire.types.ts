import { IQuestionThumbler } from '../interfaces/questionnaire/IQuestionThumbler';
import { IQuestionRadio } from '../interfaces/questionnaire/IQuestionRadio';
import { IQuestionImg } from '../interfaces/questionnaire/IQuestionImg';

export type TQuestionControl = 'radio' | 'thumbler' | 'img';
export type TQuestionLevel = 'start' | 'form' | 'result';
export type TQuestionResultState =  'default' | 'calculate' | 'complete';
export type TQuestionResultType = 'failed' | 'success';

export type TQuestionResultOption = {
    success: boolean;
    title: string
}

export type TQuestionOption = {
    title: string,
    value: any
}

export type TQuestionImgOption = {
    src: string,
    value: any
}

export type QuestionType = IQuestionThumbler | IQuestionRadio | IQuestionImg;