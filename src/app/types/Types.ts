export type Theme = 'sea' | 'coral' | 'blue';
export type TaskListType = 'todo' | 'active' | 'completed';
export type TaskStream = 'todo$' | 'active$' | 'completed$';
export type TriggerProgress = 'sub' | 'add';
export type TSide = 'left' | 'right' | 'top' | 'bottom';
export type TPosition = 'first' | 'last';
export type NumberStatus = 'positive' | 'negative';
export type TChangeSide = 'prev' | 'next';

export type Simple<T> = {
    [key:string]: T
}

export type Step = {
    value: number | string;
    circleBackground: string;
    circleBorderColor: string;
    arrowColor: string;
    text: string;
}

export type SimpleOption<T> = {
    title: string,
    value: T,
    id?: number | string
}

export type ConnectedLineMethod = 'firstLine' | 'secondLine' | 'thirdLine' | 'fourthLine';
export type RoleType = 'ADMIN' | 'MODERATOR' | 'USER' | 'GUEST';


