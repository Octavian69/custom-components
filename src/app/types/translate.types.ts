import { SimpleOption, Simple } from './Types';

export type LanguageResponse = {
    dirs: Array<string>,
    langs: Simple<string>
};
export type TranslateResponse = {
    code: number;
    lang: string;
    text: [string]
}
export type TranslateForm = {
    fromLang: string,
    toLang: string,
    text: string,
    translate: string
};

export type TranslateFormKey = keyof TranslateForm;
export type LanguageOption = Exclude<SimpleOption<string>, 'id'>;
export type LanguageCotntrol = 'fromLang' | 'toLang';

