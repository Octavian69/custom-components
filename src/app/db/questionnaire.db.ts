import { TQuestionLevel, QuestionType } from '../types/questionnaire.types';

export const questionLevels: Readonly<TQuestionLevel[]> = ['start', 'form', 'result'];
export const questions: Readonly<QuestionType[]> = [
    {
        type: 'radio',
        formControlName: 'smallpox',
        defaultValue: null,
        title: 'Who invented the smallpox vaccine',
        multiple: false,
        correctValue: 1,
        options: [
            {
                title: 'Edward Anthony Jenner',
                value: 1
            },
            {
                title: 'Paul Ehrlich',
                value: 2
            },
            {
                title: 'Karl Landsteiner',
                value: 3
            },
            {
                title: 'Karl Landsteiner',
                value: 4
            }
        ]
    },
    {
        type: 'thumbler',
        formControlName: 'antibiotics',
        defaultValue: [],
        multiple: true,
        correctValue: [1, 2, 4],
        title: 'What scientists are involved in the invention of antibiotics',
        options: [
            {
                title: 'Alexander Fleming',
                value: 1
            },
            {
                title: 'Howard Flory',
                value: 2
            },
            {
                title: 'Einthoven Willem',
                value: 3
            },
            {
                title: 'Ernst Boris Chain',
                value: 4
            },
        ]
    },
    {
        type: 'img',
        formControlName: 'bohr',
        defaultValue: null,
        title: 'Which of these people is Niels Bohr',
        multiple: false,
        correctValue: 2,
        options: [
            {
                src: '/assets/img/questionnaire/andrew-huxley.jpeg',
                value: 1
            },
            {
                src: '/assets/img/questionnaire/Niels_Bohr.jpg',
                value: 2
            },
            {
                src: '/assets/img/questionnaire/James_Whyte_Black.jpg',
                value: 3
            },
            {
                src: '/assets/img/questionnaire/ilia-mechnikov.jpg',
                value: 4
            },
        ]
    },
    {
        type: 'radio',
        formControlName: 'einstein',
        title: 'Date of birth of Albert Einstein',
        defaultValue: null,
        multiple: false,
        correctValue: 2,
        options: [
            {
                title: '15.07.1905',
                value: 1
            },
            {
                title: '14.03.1879',
                value: 2
            },
            {
                title: '01.12.1888',
                value: 3
            },
            {
                title: '22.01.1894',
                value: 4
            },
        ]
    },
]