import { DatePickerLevelType, DPSectionType } from '../types/datepicker.types';

export const Months = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
] as const;


export const dpDaysTitles = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт' , 'Пт', 'Сб'] as const;
export const DPManagersList: Readonly<DatePickerLevelType[]> = ["year", 'month', 'date'];


export const dpSectionTypes: Readonly<DPSectionType[]> = [
    {type: 'year', label: 'Год'},
    {type: 'month', label: 'Месяц'},
    {type: 'date', label: 'Число'},
];
