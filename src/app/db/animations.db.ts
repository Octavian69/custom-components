import { ANTranslatOption } from '../types/animation.types';

export const ANSideOptions: Readonly<ANTranslatOption[]> = [
    {side: 'left', status: 'negative'},
    {side: 'right', status: 'positive'},
    {side: 'top', status: 'negative'},
    {side: 'bottom', status: 'positive'},
] as const;