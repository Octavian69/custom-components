import { TranslateFormKey } from '../types/translate.types';

export const translateControlKeys: TranslateFormKey[] = ['fromLang', 'toLang', 'text'];

