import { TQuestionControl, QuestionType } from 'src/app/types/questionnaire.types';
import { QuestionThumbler } from './QuestionThumbler';
import { IQuestionThumbler } from 'src/app/interfaces/questionnaire/IQuestionThumbler';
import { QuestionRadio } from './QuestionRadio';
import { IQuestionRadio } from 'src/app/interfaces/questionnaire/IQuestionRadio';
import { QuestionImg } from './QuestionImg';
import { IQuestionImg } from 'src/app/interfaces/questionnaire/IQuestionImg';

export class QuestionFactory {
    
    constructor() {}

    create(type: TQuestionControl, question: QuestionType): QuestionType {
        switch(type) {

            case 'thumbler': {
                return new QuestionThumbler(question as IQuestionThumbler)
            }

            case 'radio': {
                return new QuestionRadio(question as IQuestionRadio)
            }

            case 'img': {
                return new QuestionImg(question as IQuestionImg)
            }
        }
    }
}