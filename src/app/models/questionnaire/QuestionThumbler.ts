import { Question } from './Question';
import { IQuestionThumbler } from 'src/app/interfaces/questionnaire/IQuestionThumbler';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

export class QuestionThumbler extends Question implements IQuestionThumbler {
    public readonly multiple: boolean;
    public readonly options: TQuestionOption[];    
}