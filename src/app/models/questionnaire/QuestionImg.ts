import { Question } from './Question'
import { IQuestionImg } from 'src/app/interfaces/questionnaire/IQuestionImg';
import { TQuestionImgOption } from 'src/app/types/questionnaire.types';

export class QuestionImg extends Question implements IQuestionImg {
    options: TQuestionImgOption[];
}