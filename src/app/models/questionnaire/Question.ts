import { IQuestion } from 'src/app/interfaces/questionnaire/IQuestion';
import { TQuestionControl, QuestionType } from 'src/app/types/questionnaire.types';

export class Question implements IQuestion {
    public readonly title: string;
    public readonly type: TQuestionControl;
    public readonly formControlName: string;
    public readonly defaultValue: string;
    public readonly correctValue: any;
    public readonly multiple: any;

    constructor(question: QuestionType) {
        Object.assign(this, question)
    }

}