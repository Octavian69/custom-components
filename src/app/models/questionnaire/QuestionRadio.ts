import { Question } from './Question';
import { IQuestionRadio } from 'src/app/interfaces/questionnaire/IQuestionRadio';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

export class QuestionRadio extends Question implements IQuestionRadio {
    public readonly options: TQuestionOption[];
}