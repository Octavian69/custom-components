import { TQuestionResultOption } from 'src/app/types/questionnaire.types';
import { IQuestionResult } from 'src/app/interfaces/questionnaire/IQuestionResult';

export class QuestionsResult implements IQuestionResult {
    
    public success: TQuestionResultOption[] = [];
    public failed: TQuestionResultOption[] = [];
    public efficiency: number = 0;

    constructor() {}

    calculateEfficiency(questionsLenght: number): void {
        const efficiency: number = (this.success.length * 100) / questionsLenght;

        this.efficiency = +efficiency.toFixed(2);
    }
}