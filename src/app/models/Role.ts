import { RoleType } from '../types/Types';

export class Role {
    constructor(
        public type: RoleType = null,
        public checked: boolean = false
    ) {}
}