import { DPStateManager } from './DPStateManager';
import { ITimepickerManager } from '../../interfaces/datepickers/ITimepickerManager';
import { TimeEvent } from '../../types/datepicker.types';
import { DATE } from '../../namespaces/namespaces';
import { copyDate } from '../../handlers/datepicker.handlers';

export class TPManager implements ITimepickerManager {

    isShowStatus: boolean = false;

    constructor(
        public dateStateManager: DPStateManager
    ) {}

    getShowState(): boolean {
        return this.isShowStatus;
    }

    setShowState(): void {
        const method: 'open' | 'close' = this.getShowState() ? 'close' : 'open';
        this[method]();
    }

    setShowStatus(flag: boolean): void {
        this.isShowStatus = flag;
    }

    open(): void {
        this.setShowStatus(true)
    }

    close(): void {
        this.setShowStatus(false);
    }

    get(): [string, string] {
        const date: Date = this.dateStateManager.get();
        const [hours, minutes] = date.toLocaleTimeString().split(':');

        return [hours, minutes];
    }

    set(event: TimeEvent): void {
        const { type, action } = event;
        const date: Date = this.dateStateManager.get();
        const updateDate: Date = copyDate(date)
        const maxLimit: number = type === 'hours' ? DATE.HOURS_MAX : DATE.MINUTES_MAX;
        const currentValue: number = type === 'hours' ? updateDate.getHours() : updateDate.getMinutes();
        const method = type === 'hours' ? 'setHours' : 'setMinutes';
        let value: number;

        switch(true) {

            case currentValue === 0 && action === 'dec': {
                value = maxLimit;
                break;
            }
            
            case currentValue === maxLimit && action === 'inc': {
                value = 0
                break;
            }

            case action === 'inc': {
                value = currentValue + 1;
                break;
            }

            case action === 'dec': {
                value = currentValue - 1;
                break
            }
        }

        updateDate[method](value);

        this.dateStateManager.setFullDate(updateDate);
    }
}