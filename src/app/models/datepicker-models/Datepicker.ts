import { DPStateManager } from './DPStateManager';
import { DPListType } from '../../types/datepicker.types';
import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';

export class DatePicker<T extends DPListType> implements Pick<IDatepickerManager<DPListType>, 'getList'> {
    public list: T;
    public value: number = null;

    constructor(
        public stateManager: DPStateManager
    ) {
    }

    getList(): T {
        return this.list;
    }

    reset(): void {
        this.value = null;
    }
}