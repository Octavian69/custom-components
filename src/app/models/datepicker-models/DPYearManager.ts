import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';
import { DatePickerOption } from './DatepcikerOption';
import { DatePickerLevelType, DPAction, DPSimpleListType, DateLimitType } from '../../types/datepicker.types';
import { DatePicker } from './Datepicker';
import { DATE } from '../../namespaces/namespaces';
import { copyDate } from '../../handlers/datepicker.handlers';

export class DPYearManager extends DatePicker<DPSimpleListType> implements IDatepickerManager<DPSimpleListType> {
    public type: DatePickerLevelType = 'year';

    init() {
        const date: Date = this.stateManager.get();
        this.complete(date.getFullYear());
    }

    set(option: DatePickerOption): void {
        const date: Date = this.stateManager.get();
        let updateDate: Date = copyDate(date);
        const { value: year } = option;

        updateDate.setFullYear(year);

        this.stateManager.setFullDate(updateDate);
        this.stateManager.setLevel('month');

    }

    change(action: DPAction): void {
        const optionIdx: number = action === 'inc' ? this.list.length - 1 : 0;
        const { value }: DatePickerOption = this.list[optionIdx];
        const year = action === 'inc' ? value + 1 : value - DATE.YEAR_LIST_LENGTH;
        
        this.complete(year);
    }

    complete(year: number): void {
        const list: DatePickerOption[] = [];
        const limit: number = DATE.YEAR_LIST_LENGTH;
        const date: Date = this.stateManager.get();
        const minYear: number = this.getLimitDate('min');
        const maxYear: number = this.getLimitDate('max');

        for(let i = 0; i < limit; i++, year++) {
            const current: boolean = date.getFullYear() === year;
            const disabled: boolean = year < minYear || year > maxYear;
            const option = new DatePickerOption(year, current, disabled);
            list.push(option);
        }

        this.list = list;
    }

    getLimitDate(type: DateLimitType): number {
        const limitDate: Date = this.stateManager.getLimitDate(type);
        const defaultValue: number = type === 'min' ? -1 : Infinity;

        if(limitDate) return limitDate.getFullYear();

        return defaultValue; 
    }
}