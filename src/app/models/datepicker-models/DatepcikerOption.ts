import { Month, DayTitle } from '../../types/datepicker.types';

export class DatePickerOption {
    constructor(
        public value: number,
        public current: boolean = false,
        public disabled: boolean = false,
        public label: Month | DayTitle = null
    ) {}
}