import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';
import { DPManagerFactory } from './DPManagerFactory';
import { DPManagersList } from '../../db/datepicker.db';
import { DatePickerLevelType, DPAction, DPListType, DateLimitType } from '../../types/datepicker.types';
import { IDPStateLeveles } from '../../interfaces/datepickers/IDPStateLeveles';
import { IDPStateManager } from '../../interfaces/datepickers/IDPStateManager';
import { DatePickerOption } from './DatepcikerOption';
import { copyDate } from '../../handlers/datepicker.handlers';

export class DPStateManager implements IDPStateManager {
    
    isShowStatus: boolean = false; 
    state: IDPStateLeveles;
    level: IDatepickerManager<DPListType>;
    disabled: boolean = false;

    constructor(
        public date: Date,
        public minDate: Date = null,
        public maxDate: Date = null
    ) {
        this.init();
    }

    init(): void {
        this.date = this.checked(this.date);

        const factory: DPManagerFactory = new DPManagerFactory();
        const state = DPManagersList.reduce((accum: IDPStateLeveles, type: DatePickerLevelType) => {
            accum[type] = factory.create(type, this);

            return accum;
        }, {} as IDPStateLeveles);

        this.state = state;
    }

    checked(date: Date): Date {

        const isLess: boolean = this.minDate && this.minDate >= date;
        const isMore: boolean = this.maxDate && this.maxDate <= date;

        if(isLess) return copyDate(this.minDate);
        if(isMore) return copyDate(this.maxDate);

        return copyDate(date);
    }

    get(): Date {
        return this.date;
    }

    getLevel(level: DatePickerLevelType): IDatepickerManager<DPListType> {
        return this.state[level];
    }

    getLimitDate(type: DateLimitType): Date {
        return type === 'min' ? this.minDate : this.maxDate;
    }

    setLimitDate(type: DateLimitType, date: Date): void {

        const update: Date = date ? copyDate(date) : null;

        type === 'min' ? this.minDate = update : this.maxDate = update;
    }

    set(option: DatePickerOption) {
        this.level.set(option);
    }

    setFullDate(date: Date): void {
        const updateDate: Date = date || new Date;

        this.date = this.checked(updateDate);

        const [hours, minutes] = this.date.toLocaleTimeString().split(':');

        this.date.setHours(+hours, +minutes, 0, 0);
    }

    setLevel(type: DatePickerLevelType): void {
        setTimeout(_ => {
            this.level = this.state[type];
            this.state[type].init();
        }, 100);
    }

    complete(param: number): void {
        this.level.complete(param);
    }

    change(action: DPAction): void {
        this.level.change(action);
    }

    next(key: DatePickerLevelType): void {
        this.level = this.state[key];
        this.level.init();
    }

    getList(): DPListType {
        return this.level.getList();
    }

    getShowState(): boolean {
        return this.isShowStatus;
    }

    setShowStatus(flag: boolean): void {
        this.isShowStatus = flag;
    }

    setShowState(): void {
        const method: 'open' | 'close' = this.getShowState() ? 'close' : 'open';
    
        this[method]();
      }

    open(): void {
        this.next('year');
        this.setShowStatus(true);
    }

    close(): void {
        this.setShowStatus(false);
    }

    reset(): void {
        Object.values(this.state).forEach(level => level.reset());
        this.close();
    }
}