import { DatePickerLevelType, DPDateListType, DPSimpleListType } from '../../types/datepicker.types';
import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';
import { DPYearManager } from './DPYearManager';
import { DPMonthsManager } from './DPMonthsManager';
import { DPDateManager } from './DPDateManager';
import { DPStateManager } from './DPStateManager';

export class DPManagerFactory {
    create(type: DatePickerLevelType, stateManager: DPStateManager) {
     
        switch(type) {
            case 'date': {
                const datesManager: IDatepickerManager<DPDateListType> = new DPDateManager(stateManager);

                return datesManager;
            }

            case 'month': {
                const monthManager: IDatepickerManager<DPSimpleListType> = new DPMonthsManager(stateManager);

                return monthManager;
            }

            case 'year': {
                const yearManager: IDatepickerManager<DPSimpleListType> = new DPYearManager(stateManager);

                return yearManager;
            }
        }
    }
}