import { DatePicker } from './Datepicker';
import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';
import { DatePickerLevelType, DayTitle, DPDateListType, DateLimitType } from '../../types/datepicker.types';
import { dpDaysTitles } from '../../db/datepicker.db';
import { copyDate as DateCopy } from '../../handlers/datepicker.handlers';
import { DatePickerOption } from './DatepcikerOption';

export class DPDateManager extends DatePicker<DPDateListType> implements IDatepickerManager<DPDateListType> {
    type: DatePickerLevelType = 'date';
    
    init(): void {
        const date: Date = this.stateManager.get();
        const copyDate: Date = DateCopy(date);
        const valueDate: number = copyDate.getDate();
        const currentMonth: number = copyDate.getMonth();
        const minDate: number = this.getLimitDate('min');
        const maxDate: number = this.getLimitDate('max');
        const weeks:  DatePickerOption[][] = new Array(6).fill(null).map(_ => []);
        let weekIdx: number = 0;
        copyDate.setDate(1);

        const firstWeekDay = copyDate.getDay();

        //  (декабрь 2024)

        if(firstWeekDay !== 1) {
            const offsetFirstWeek: number = firstWeekDay === 0 ? 6 : firstWeekDay - 1;
            copyDate.setDate(1 - offsetFirstWeek);
        }
        
        //пока последняя неделя не наполнится до 7 дней
        while(weeks[weeks.length - 1].length !== 7) {
            const currentDay: number = copyDate.getDay();
            const currentDate: number = copyDate.getDate();
            const disbaled: boolean = currentMonth !== copyDate.getMonth() || currentDate < minDate || currentDate > maxDate;
            // || currentDate > maxDate
            const isCurrent: boolean = valueDate === currentDate && !disbaled;
            const label: DayTitle = dpDaysTitles[currentDay]
            const option: DatePickerOption = new DatePickerOption(currentDate, isCurrent, disbaled, label)
            const isSunday: boolean = currentDay === 0;

            weeks[weekIdx].push(option);
            copyDate.setDate(currentDate + 1);

            if(isSunday) weekIdx++;
        }

       this.list = weeks;
    }

    set(option: DatePickerOption): void {
        const { value: date } = option;
        const value: Date = this.stateManager.get();

        value.setDate(date);
        this.value = date;
        this.stateManager.setFullDate(value);
        
        this.list.forEach((week: DatePickerOption[]) => {
            week.forEach((day: DatePickerOption) => {
                day.current = day.value === date && !day.disabled;
            })
        }); 
    }

    getLimitDate(type: DateLimitType): number {
        const limitDate: Date = this.stateManager.getLimitDate(type);
        const defaultValue: number = type === 'min' ? -1 : Infinity;

        if(limitDate) {
            const currentDate: Date = this.stateManager.get();
            const currenYear: number = currentDate.getFullYear()
            const currentMonth: number = currentDate.getMonth();
            const minYear: number = limitDate.getFullYear();
            const minMonth: number = limitDate.getMonth();
            const min: number = limitDate.getDate();

            const isCurrentDate: boolean = Object.is(currenYear, minYear) && Object.is(currentMonth, minMonth);

            return isCurrentDate ? min : defaultValue;
        }

        return defaultValue;
    }
}