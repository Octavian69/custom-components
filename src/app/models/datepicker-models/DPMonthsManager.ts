import { IDatepickerManager } from '../../interfaces/datepickers/IDatepickerManager';
import { DatePicker } from './Datepicker';
import { DatePickerLevelType, DPAction, DPSimpleListType, DateLimitType } from '../../types/datepicker.types';
import { Months } from '../../db/datepicker.db';
import { DatePickerOption } from './DatepcikerOption';

export class DPMonthsManager extends DatePicker<DPSimpleListType> implements IDatepickerManager<DPSimpleListType> {
    type: DatePickerLevelType = 'month';

    init() {
        const date: Date = this.stateManager.get();
        const currentMonth: number = date.getMonth();
        const minMonth: number = this.getLimitDate('min');
        const maxMonth: number = this.getLimitDate('max');

        const list: DatePickerOption[] = Months.map((month, i: number) => {
            const current: boolean = i === currentMonth;
            const disabled: boolean = i < minMonth || i > maxMonth

            return new DatePickerOption(i, current, disabled, month);
        });

        this.list = list;
    }

    set(option: DatePickerOption): void {
        const date: Date = this.stateManager.get();
        const updateDate: Date = new Date(date);
        const { value: month } = option;

        updateDate.setMonth(month);

        const isEqualDate: boolean = updateDate.getDate() === date.getDate();

        if(!isEqualDate) {
            updateDate.setDate(0);
        }
        
        this.value = month;
        this.stateManager.setFullDate(updateDate);
        this.stateManager.setLevel('date');
    }

    getLimitDate(type: DateLimitType): number {
        const currentDate: Date = this.stateManager.get();
        const limitDate: Date = this.stateManager.getLimitDate(type);
        const defaultValue: number = type === 'min' ? -1 : Infinity;

        if(limitDate) {
            const currentYear: number = currentDate.getFullYear();
            const limitYear: number = limitDate.getFullYear();
            const limitMonth: number = limitDate.getMonth();

            return currentYear === limitYear ? limitMonth : defaultValue;
        }

        return defaultValue;
    }
}