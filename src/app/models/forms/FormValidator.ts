import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

export class FormValidator {
    static compose(correctValue: any | any[], multiple: boolean): ValidatorFn[] {
        const method: 'correct' | 'correctMultiple' = multiple ? 'correctMultiple' : 'correct';

        return [FormValidator[method](correctValue)]
    }

    static asyncCompose() {

    }

    static correct(correctValue: any): ValidatorFn {

        return (formControl: AbstractControl ): ValidationErrors => {
            const { value } = formControl;

            return value !== correctValue ? { failed: true } : null;
        }
        

    }

    static correctMultiple(correctValue: any[]): ValidatorFn {

        return (formControl: AbstractControl): ValidationErrors => {
            const { value } = formControl;
            const isCorrectLength: boolean = value && value.length === correctValue.length;
    
            if(isCorrectLength) {
                const isCorrect: boolean = value.every(v => correctValue.includes(v));
    
                return isCorrect ? null : { failed: true };
            }
            return { failed: true };
        }
    }
}