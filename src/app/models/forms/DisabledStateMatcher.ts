import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl } from '@angular/forms';

export class DisabledStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl): boolean {
        return null;
    }
}