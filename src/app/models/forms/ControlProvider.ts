import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef, Type } from '@angular/core';

export class ControlProvider<T> {
    public provide = NG_VALUE_ACCESSOR;
    public useExisting: Type<T>;
    public multi: boolean = true;

    constructor(public constr: T) {
        this.useExisting = forwardRef(() => constr);
    }
}