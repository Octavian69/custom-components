import { CdkDropList } from '@angular/cdk/drag-drop';
import { TaskListType } from '../types/Types';

export class DropList {
    constructor(
        public type: TaskListType,
        public dropList: CdkDropList
    ) {}
}