export namespace DATE {
    export const YEAR_LIST_LENGTH: number = 9;
    export const HOURS_MAX = 23;
    export const MINUTES_MAX = 59;
}