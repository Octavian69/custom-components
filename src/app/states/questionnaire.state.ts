import { TSide } from '../types/Types';
import { TQuestionResultState } from '../types/questionnaire.types';

export class QuestionnaireState {
    public showFormControlSide: Exclude<TSide, 'top' | 'bottom'>  = 'right';
    public resultState: TQuestionResultState = 'default';

    constructor() {}
}