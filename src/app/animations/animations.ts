import { trigger, transition, style, animate, state, AnimationAnimateMetadata, AnimationStyleMetadata, AnimationTransitionMetadata } from '@angular/animations';
import { AxisType, ANTranslateType, ANTranslatOption } from '../types/animation.types';
import { ANSideOptions } from '../db/animations.db';

export const fade = trigger('fade', [
    transition(':enter', [
        style({ opacity: 0, display: 'block' }),
        animate(700, style({ opacity: 1 }))
    ]),
    transition(':leave', [
        style({ opacity: '1' }),
        animate(300, style({ opacity: 0 }))
    ])
])

export const width = trigger('width', [
    transition(':enter', [
        style({ width: 0, height: '*' }),
        animate(400, style({ width: '*', height: '*'  }))
    ]),
    transition(':leave', [
        style({ width: '*', height: '*'  }),
        animate(400, style({ width: '0', height: '*'  }))
    ])
]);

export const ANTranslate = (fromValue: string, toValue: string, time: number, axis: AxisType = '') =>{
    
    const property: string = `translate${axis}`;

    return trigger('translate', [
        transition(':enter', [
            style({
                opacity: 0,
                transform: `${property}(${fromValue})`
            }),
            animate(time, style({
                opacity: 1,
                transform: `${property}(${toValue})`
            }))
        ]),
        transition(':leave', [
            animate(time, style({
                opacity: 0,
                transform: `${property}(${fromValue})`
            }))
        ])
    ]);
}

export const ANScale = (ms: number = 300) => trigger('scale', [
    transition(':leave', animate(ms, style({ transform: 'scale(0)', opacity: 0 }))),
])

export const ANToogleSize = (prop: 'height' | 'width') => trigger('toggleSize', [
    state('open', style({
        [prop]: '*'
    })),
    state('close', style({
        [prop]: '0px'
    })),
    transition('open <=> close', animate(400))
]);

export const ANScaleState = (ms: number = 300, fromState: number = 0) => trigger('ANScaleState', [
    transition('*<=>*', [
        style({
            opacity: 0,
            transform: `scale(${fromState})`
        }),
        animate(ms, style({
            opacity: 1,
            transform: 'scale(1)'
        }))
    ])
]);

export const ANVoidTranslate = (ms: number = 300, fromValue: string = '100%') => {
    const transitions: AnimationTransitionMetadata[] = ANSideOptions.map((option: Readonly<ANTranslatOption>) => {
        const { side, status } = option;
        const transitionState: string = `void => ${side}`;
        const axis: AxisType = side === 'top' || side === 'bottom' ? 'Y' : 'X';
        const prop = `translate${axis}` as ANTranslateType;
        const value: string = status === 'negative' ? `-${fromValue}` : fromValue;

        const stylesState: [AnimationStyleMetadata, AnimationAnimateMetadata] = [
            style({
                display: 'block',
                opacity: "0",
                transform: `${prop}(${value})`
            }),
            animate(ms, style({
                opacity: "1",
                transform: `${prop}(0%)`
            }))
        ]

        return transition(transitionState, stylesState);
    });

    return trigger('ANVoidTranslate', transitions)
}

