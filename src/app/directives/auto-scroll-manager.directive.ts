import { Directive, ElementRef, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[uiAutoScrollManager]',
  host: {}
})
export class AutoScrollManagerDirective implements OnInit, OnDestroy {

  observer: MutationObserver;

  constructor(
    private elem: ElementRef
  ) { }

  ngOnInit() {
    this.initObserver();
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  initObserver(): void {
    const { nativeElement } = this.elem;

    this.observer = new MutationObserver(this.observerCb);
    this.observer.observe(nativeElement, { childList: true });
  }

  observerCb = (mutations: MutationRecord[]) => {
      const { nativeElement } = this.elem;
      const { scrollTop, scrollHeight, clientHeight } = nativeElement;

      const offset: number = scrollHeight - clientHeight;

      if(scrollTop < offset) {
        nativeElement.scrollTop = offset;
      }
      
  }

}
