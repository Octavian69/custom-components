import { Directive, ElementRef, OnInit, Renderer2, AfterViewInit, OnDestroy, Host, Optional, ViewChild, Input } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[uiTheme]',
  exportAs: 'themeRef'
})
export class ThemeDirective implements OnInit, AfterViewInit, OnDestroy {

  destoy$: Subject<true> = new Subject();
  color$: Observable<string> = this.themeService.color$.asObservable();

  constructor(
    private renderer: Renderer2,
    private themeService: ThemeService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.color$.pipe(
      takeUntil(this.destoy$)
    ).subscribe((color: string) => {
       this.renderer.setStyle(document.body, 'background', color);
    });
  }

  ngOnDestroy() {
    this.destoy$.next(true)
  }

  setColor() {
    this.themeService.setColor();
  }

}
