import { Directive, Input, ElementRef, OnInit, Renderer2, OnDestroy } from '@angular/core';

@Directive({
  selector: '[uiLazyFile]'
})
export class LazyFileDirective implements OnInit, OnDestroy {

  observer: IntersectionObserver;

  @Input('data-filename') filename: string;

  constructor(
    private elem: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.initObserver()
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  initObserver(): void {
    const { nativeElement } = this.elem;

    this.observer = new IntersectionObserver(this.observeCb, { threshold: 0.1 });

    this.observer.observe(nativeElement);
  }
  
  observeCb = (entries: IntersectionObserverEntry[]): void => {
      const { intersectionRatio } = entries[0];
      const { nativeElement } = this.elem;

      if(intersectionRatio > 0 && !nativeElement.src) {
        const path: string = `assets/${this.filename}`;

        this.renderer.setAttribute(nativeElement, 'src', path);
        this.renderer.setStyle(nativeElement, 'visibility', 'visible');
      }
  }
}
