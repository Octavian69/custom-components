import { Directive, OnInit, OnDestroy, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[uiBreakPage]'
})
export class BreakPageDirective implements OnInit, OnDestroy {

  observer: IntersectionObserver;

  @Output('breakPage') breakPage = new EventEmitter<string>();

  constructor(
    private elem: ElementRef
  ) { }

  ngOnInit() {
    this.initObserver();
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  initObserver(): void {
    const { nativeElement } = this.elem;
    
    this.observer = new IntersectionObserver(this.observerCb, { threshold: 0.1 });

    this.observer.observe(nativeElement);
  }

  observerCb = (entries: IntersectionObserverEntry[]): void => {
    const { intersectionRatio }  = entries[0];

    if(intersectionRatio > 0) {
      this.breakPage.emit('end page');
    }
  }

}
