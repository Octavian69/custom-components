import { Injectable } from "@angular/core";
import { ServicesModule } from '../modules/services.module';
import { Observable, BehaviorSubject } from 'rxjs';
import { TaskStream, TaskListType, Simple } from '../types/Types';
import { CdkDropList } from '@angular/cdk/drag-drop';
import { DropList } from '../models/DropList';
import { DropListItems, DropReplaceItems, DropReplaceKey, DropReplaceItem } from '../types/drag-drop.types';

@Injectable({
    providedIn: ServicesModule
})
export class DragDropService {
    replaceValues: DropReplaceItems = {
        'todo-active': { from: 'Learn', to: 'Repeat' },
        'active-todo': { from: 'Repeat', to: 'Learn' }
    }

    connected: Simple<string[]> = {
        todo: ['active'],
        active: ['todo', 'completed'],
        completed: ['active']
    }

    dropLists: DropList[] = [];

    lists: DropListItems = {
        todo: [
            'Learn Javascript',
            'Learn Angular',
            'Learn NodeJS'
        ],
        active: [
            'Repeat CDK Drag-&-Drop',
            'Repeat Angular Material',
            'Repeate JS Events'
        ],
        completed: []
    }

    listKeys: Map<string[], TaskListType> = new Map();
    
    todo$: BehaviorSubject<string[]> = new BehaviorSubject(this.lists.todo);
    active$: BehaviorSubject<string[]> = new BehaviorSubject(this.lists.active);
    completed$: BehaviorSubject<string[]> = new BehaviorSubject(this.lists.completed);

    getStream(listType: TaskListType): Observable<any> {
        const streamName = `${listType}$` as TaskStream;

        return this[streamName].asObservable();
    }

    getValue(listType: TaskListType): string[] {
        const streamName = `${listType}$` as TaskStream;
        
        return this[streamName].getValue();
    }

    registerDropList(dropList: CdkDropList, type: TaskListType): void {
        const list: DropList = new DropList(type, dropList);
        this.listKeys.set(this.lists[type], type)
        
        this.dropLists.push(list);
    }

    getConnectedCdkLists(listType: TaskListType): CdkDropList[] {
        const connectedTypes: string[] = this.connected[listType];

        const connectedLists: CdkDropList[] = this.dropLists
        .filter((list) => connectedTypes.includes(list.type))
        .map(list => list.dropList);

        return connectedLists;
    }

    replaceValue(fromData: string[], toData: string[], itemIdx: number): void {
        const toKey: TaskListType = this.listKeys.get(toData);
        const fromKey: TaskListType = this.listKeys.get(fromData);
        const replaceKey = `${fromKey}-${toKey}` as DropReplaceKey;
        const replaceItem: DropReplaceItem = this.replaceValues[replaceKey];

        if(replaceItem) {
            const { from, to } = replaceItem;
            const streamName: string = `${toKey}$`;

            toData[itemIdx] = toData[itemIdx].replace(from, to);
            this[streamName].next(toData);
        }
    }
}