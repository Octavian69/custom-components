import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import {environment as env} from '@env/environment';
import { Simple } from '../types/Types';
import { LanguageOption, LanguageResponse, TranslateForm, TranslateResponse } from '../types/translate.types';
import { map, debounceTime, switchMap, tap, takeUntil } from 'rxjs/operators';
import { ServiceManager } from '../managers/ServiceManager';
import { TranslateFlags } from '../flags/translate.flags';

@Injectable({
    providedIn: ServicesModule
})
export class TranslateService extends ServiceManager<TranslateFlags, null> {
    
    requestTranslate$: Subject<TranslateForm> = new Subject();
    languages$: BehaviorSubject<LanguageOption[]> = new BehaviorSubject(null);
    translate$: BehaviorSubject<string> = new BehaviorSubject(null);
    subs$: Subscription[] = []
 
    constructor(
        private http: HttpClient
    ) {
        super(new TranslateFlags, null)
    }

    initLanguages(): void {
        const fromObject: Simple<string> = {
            key: env.API_YA_KEY,
            ui: 'ru'
        }
        const params: HttpParams = new HttpParams({fromObject});
        const url: string = `${env.YA_TRANSLATE_URL}/tr.json/getLangs`;

        this.http.get<LanguageResponse>(url, {params})
        .pipe(
            map(({ langs }) => {
                const languages: LanguageOption[] = Object.entries(langs)
                .reduce((accum: LanguageOption[], [value, title]) => {
                    accum.push({ value, title } as LanguageOption);
    
                    return accum;
                }, []);

                languages.sort((a, b) => a.title > b.title ? 1 : -1);

                return languages;
        }))
        .subscribe((languages: LanguageOption[]) => {
            this.languages$.next(languages);
        })
    }

    initTranslateSub(): void {
        const url: string = `${env.YA_TRANSLATE_URL}/tr.json/translate`;

        const sub$: Subscription = this.getStream('requestTranslate$')
        .pipe(
            debounceTime(600),
            tap(() => this.setFlag('isTranslateRequest', true)),
            switchMap((formValue: TranslateForm) => {
                const { fromLang, toLang, text } = formValue;

                const fromObject: Simple<string> = {
                    key: env.API_YA_KEY,
                    text,
                    lang: `${fromLang}-${toLang}`
                }
                const params: HttpParams = new HttpParams({fromObject});

                return this.http.get(url, { params });
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe((res: TranslateResponse) => {
            const { text } = res;
            this.translate$.next(text[0]);
            this.setFlag('isTranslateRequest', false)
        })

        this.subs$.push(sub$);
    }
}