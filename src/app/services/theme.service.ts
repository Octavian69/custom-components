import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';
import { BehaviorSubject } from 'rxjs';

@Injectable({providedIn: ServicesModule})
export class ThemeService {
    colors: string[] = [
        'linear-gradient(to right, rgb(236, 233, 230), rgb(255, 255, 255))',
        'linear-gradient(to right, rgb(255, 239, 186), rgb(255, 255, 255))',
        'linear-gradient(to right, rgb(211, 204, 227), rgb(233, 228, 240))',
        'linear-gradient(to right, rgb(173, 169, 150), rgb(242, 242, 242), rgb(219, 219, 219), rgb(234, 234, 234))',
        'linear-gradient(to right, rgb(201, 214, 255), rgb(226, 226, 226))',
        'linear-gradient(to right, rgb(217, 167, 199), rgb(255, 252, 220))',
        'linear-gradient(to right, rgb(67, 198, 172), rgb(248, 255, 174))'
    ]

    color$: BehaviorSubject<string> = new BehaviorSubject(this.getDefaultColor());
    
    setColor() {
        const currentColor: string = this.color$.getValue();
        const { length } = this.colors;
        const currentIndex: number = this.colors.findIndex(c => c === currentColor);
        const idx: number = currentIndex === length - 1 ? 0 : currentIndex + 1;
        const nextColor: string = this.colors[idx];

        this.color$.next(nextColor);
        window.localStorage.setItem('theme', nextColor);
    }

    getDefaultColor(): string {
        return localStorage.getItem('theme') || this.colors[0];
    }

} 