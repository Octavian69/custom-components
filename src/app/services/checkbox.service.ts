import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';
import { Role } from '../models/Role';
import { RoleType } from '../types/Types';
import { BehaviorSubject } from 'rxjs';
import { ServiceManager } from '../managers/ServiceManager';

@Injectable({
    providedIn: ServicesModule
})
export class CheckboxService extends ServiceManager<null, null> {

    roles$: BehaviorSubject<Role[]> = new BehaviorSubject(this.roles);

    get roles(): Role[] {
        const userRoleTypes: RoleType[] = ['ADMIN', 'MODERATOR', 'USER', 'GUEST'];
        let checked: boolean = false;
        const roles: Role[] = userRoleTypes.map((role: RoleType) => {
            checked = !checked
            return new Role(role, checked);
        })

        return roles;
    }

    remove(role: Role): void {
        const roles: Role[] = this.getStreamValue('roles$');
        const updateRoles: Role[] = roles.filter(r => r.type !== role.type);

        this.roles$.next(updateRoles);
    }
}