import { Injectable } from '@angular/core';
import { ServicesModule } from '../modules/services.module';
import { BehaviorSubject } from 'rxjs';
import { Step } from '../types/Types';
import { ServiceManager } from '../managers/ServiceManager';

@Injectable({
    providedIn: ServicesModule
})
export class InfoStepperService extends ServiceManager<null, null> {
    
    steps: Step[] = [
        {
            value: 'Click!',
            circleBackground: 'dodgerblue',
            circleBorderColor: 'darkblue',
            arrowColor: 'powderblue',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, consectetur?'
        },
        {
            value: 2500,
            circleBackground: 'orange',
            circleBorderColor: 'orangered',
            arrowColor: 'tomato',
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi sunt unde optio pariatur alias impedit.'
        },
        {
            value: 3500,
            circleBackground: 'darkgreen',
            circleBorderColor: 'lightgreen',
            arrowColor: 'greenyellow',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam consectetur iure beatae dolorem et totam, labore alias soluta. Commodi, pariatur?'
        },
        {
            value: 4500,
            circleBackground: 'antiquewhite',
            circleBorderColor: 'lightsalmon',
            arrowColor: 'lightpink',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente unde iure quam excepturi sed aliquam illum eos odit nihil doloribus!'
        }
    ];
    
    steps$: BehaviorSubject<Step[]> = new BehaviorSubject(this.steps);
    

    constructor() {
        super();
    }

    
}