import { Injectable } from "@angular/core";
import { ServicesModule } from '../modules/services.module';
import { ServiceManager } from '../managers/ServiceManager';
import { QuestionType, TQuestionLevel } from '../types/questionnaire.types';
import { questions, questionLevels } from '../db/questionnaire.db';
import { BehaviorSubject, of, timer } from 'rxjs';
import { takeUntil, map, delay, switchMapTo } from 'rxjs/operators';
import { QuestionnaireFlags } from '../flags/questionnaire.flags';
import { QuestionnaireState } from '../states/questionnaire.state';
import { QuestionFactory } from '../models/questionnaire/QuestionFactrory';
import { TChangeSide, TSide } from '../types/Types';
import { FormGroup, FormControl } from '@angular/forms';
import { IQuestionResult } from '../interfaces/questionnaire/IQuestionResult';
import { QuestionsResult } from '../models/questionnaire/QuestionsResult';

@Injectable({
    providedIn: ServicesModule
})
export class QuestionnaireService extends ServiceManager<QuestionnaireFlags, QuestionnaireState> {
    public levels: Readonly<TQuestionLevel[]> = questionLevels;

    public level$: BehaviorSubject<TQuestionLevel> = new BehaviorSubject('start');
    public questions$: BehaviorSubject<Readonly<QuestionType[]>> = new BehaviorSubject(null);
    public question$: BehaviorSubject<QuestionType> = new BehaviorSubject(null);
    public form$: BehaviorSubject<FormGroup> = new BehaviorSubject(null);
    public formValue$: BehaviorSubject<any> = new BehaviorSubject(null);
    public results$: BehaviorSubject<IQuestionResult> = new BehaviorSubject(null);
    
    constructor() {
        super(new QuestionnaireFlags, new QuestionnaireState);
    }

    fetch(): void {
        
        this.setFlag('isQuestionsRequest', true);

        of(questions)
        .pipe(
            delay(1000),
            map((qstns: QuestionType[]) => {
                const factory: QuestionFactory = new QuestionFactory();

                return qstns.map(q => factory.create(q.type, q));
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe((qstns: Readonly<QuestionType[]>) => {
            this.questions$.next(qstns);
            this.question$.next(qstns[0]);
            this.setFlag('isLoadQuestions', true);
            this.setFlag('isQuestionsRequest', false);
            this.changeLevel('form');
        })
    }

    changeLevel(level: TQuestionLevel): void {
        this.level$.next(level);
    }

    changeQuestion(side: TChangeSide): void {
        this.setFlag('isSetQuestion', true);

        const questions: Readonly<QuestionType[]> = this.questions$.getValue();
        const currentQuestion: QuestionType = this.question$.getValue();
        const currentIdx: number = questions.indexOf(currentQuestion);
        const nextIdx: number = side === 'prev' ? currentIdx - 1 : currentIdx + 1;
        const nextQuestion: QuestionType = questions[nextIdx];
        const showSide: Exclude<TSide, 'top' | 'bottom'> = side === 'prev' ? 'left' : 'right'; 
        
        if(nextQuestion) {
            this.setStateProp('showFormControlSide', showSide);
            this.question$.next(nextQuestion);
        } 
    }

    submit(form: FormGroup): void {
        this.results$.next(null);
        this.form$.next(form);
        this.formValue$.next(form.getRawValue());
        this.changeLevel('result');
    }

    start(): void {
        // if(this.getFlag('isQuestionsRequest')) return
        this.setStateProp('showFormControlSide', 'right');

        if(!this.getFlag('isLoadQuestions')) {
            this.fetch();
        } else {
            this.changeLevel('form')
        }
    }

    restart(): void {
        const [question] = this.questions$.getValue(); //1-ый вопрос
        this.question$.next(question);
        this.changeLevel('start');
        this.formValue$.next(null);
        this.form$.next(null);
        this.results$.next(null);
        this.setStateProp('resultState', 'default');
    }

    calculate(): void {
        this.setStateProp('resultState', 'calculate');

        const questions: Readonly<QuestionType[]> = this.questions$.getValue();
        const form: FormGroup = this.form$.getValue();

        const { controls } = form;
    
        const results: QuestionsResult = Object.entries(controls).reduce((accum, current: [string, FormControl]) => {
            const [ formControlName, formControl ] = current;
            const { valid: success } = formControl;
            const { title } = questions.find(q => q.formControlName === formControlName);
    
            if(success) accum.success.push({ success, title })
            else accum.failed.push({ success, title })
    
            return accum;
    
        }, new QuestionsResult);
        
        results.calculateEfficiency(questions.length);


        timer(2000).subscribe(_ => {
            this.results$.next(results);
            this.setStateProp('resultState', 'complete');
        })
    }
}