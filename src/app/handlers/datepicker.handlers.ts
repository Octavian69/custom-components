import { DayTitle } from '../types/datepicker.types';
import { dpDaysTitles } from '../db/datepicker.db';
import { DatePickerOption } from '../models/datepicker-models/DatepcikerOption';

export function disableDatesDecorator (firstWeek: DatePickerOption[], date: Date) {
    const copyDate = new Date(date);

    copyDate.setDate(0);

    while(firstWeek.length !== 7) {
        const date: number = copyDate.getDate();
        const day: number = copyDate.getDay()
        const label: DayTitle = dpDaysTitles[day];
        const option: DatePickerOption = new DatePickerOption(date, false, true, label)
        const prevDate = date - 1;

        copyDate.setDate(prevDate);
        firstWeek.unshift(option);
    }

    return firstWeek;

}

export const copyDate: (date: Date) => Date = (date: Date): Date => new Date(date);
