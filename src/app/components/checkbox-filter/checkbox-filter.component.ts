import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CheckboxService } from 'src/app/services/checkbox.service';
import { Observable } from 'rxjs';
import { Role } from 'src/app/models/Role';

@Component({
  selector: 'ui-checkbox-filter',
  templateUrl: './checkbox-filter.component.html',
  styleUrls: ['./checkbox-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxFilterComponent {
  
  roles$: Observable<Role[]> = this.checkboxService.getStream('roles$');

  constructor(
    private checkboxService: CheckboxService
  ) { }

  remove(role: Role) {
    this.checkboxService.remove(role)
  }

}
