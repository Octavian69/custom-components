import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-percent-popup',
  templateUrl: './percent-popup.component.html',
  styleUrls: ['./percent-popup.component.scss']
})
export class PercentPopupComponent implements OnInit {

  offset: string = '0';

  constructor() { }

  ngOnInit(): void {}
  

  setValue(e: MouseEvent): void {
    const elem: HTMLElement = e.currentTarget as HTMLElement;
    const { clientX } = e;
    const { offsetWidth, offsetLeft } = elem;
    // const { left } = elem.getBoundingClientRect()

    const offset: number = ((clientX - offsetLeft) * 100) / offsetWidth;

    this.offset = offset < 0 ? '0' : offset.toFixed(0);
  }

}
