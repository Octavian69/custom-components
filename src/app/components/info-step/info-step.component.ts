import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Step, Simple } from 'src/app/types/Types';
import { width } from 'src/app/animations/animations';

@Component({
  selector: 'ui-info-step',
  templateUrl: './info-step.component.html',
  styleUrls: ['./info-step.component.scss'],
  animations: [width]
})
export class InfoStepComponent implements OnInit {

  isShowText: boolean = false;

  @Input('step') step: Step;
  @Input('index') index: number;
  @Input('length') length: number;

  constructor() { }

  ngOnInit(): void {
  }

  getStepPositionStyle(): Simple<string> {
    const offset = (this.length - this.index) * 17; 

    return {
      transform: `translateX(${offset}px)`
    }
  }

  getRowColor(): Simple<string> {
    
    const { arrowColor: backgroundColor } = this.step;

    return { backgroundColor };
  }

  getCircleColor(): Simple<string> {
    
    const { circleBackground: backgroundColor, circleBorderColor } = this.step;

    return {
      backgroundColor,
      border: `5px solid ${circleBorderColor}`
    }
  }

  getTriangleColor(side: 'left' | 'right' = 'right'): Simple<string>  {
    const { arrowColor: color } = this.step;

    const styles: Simple<string> = 
    side === 'right' 
    ? {borderLeftColor: color}
    : { 
      borderTopColor: color,
      borderBottomColor: color
     }

     return styles;

  }

  setTextState(): void {
    this.isShowText = !this.isShowText;
  }

}
