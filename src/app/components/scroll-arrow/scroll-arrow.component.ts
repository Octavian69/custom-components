import { Component, HostListener } from '@angular/core';
import { ANTranslate } from 'src/app/animations/animations';

@Component({
  selector: 'ui-scroll-arrow',
  templateUrl: './scroll-arrow.component.html',
  styleUrls: ['./scroll-arrow.component.scss'],
  animations: [ANTranslate('-1rem', '0rem', 500, "Y")]
})
export class ScrollArrowComponent {

  isShow: boolean = false;

  @HostListener('window:scroll')
    scroll(): void {
      const { scrollTop } = document.documentElement;

      if(scrollTop >= 500 && !this.isShow) this.isShow = true;

      if(scrollTop < 500 && this.isShow) this.isShow = false;
    }

  scrollTop(): void {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }

  done(e) {
    // console.log(e)
  }
}
