import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { TriggerProgress } from 'src/app/types/Types';

@Component({
  selector: 'ui-progress-trigger',
  templateUrl: './progress-trigger.component.html',
  styleUrls: ['./progress-trigger.component.scss']
})
export class ProgressTriggerComponent implements OnInit, OnDestroy {

  width: number = 100;
  value: number = this.width;
  trigger$: Subject<TriggerProgress> = new Subject();
  sub$: Subscription;

  constructor(private zone: NgZone) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    if(this.sub$) this.sub$.unsubscribe();
  }

  init(): void {
    this.zone.runOutsideAngular(() => {
      let interval$;

      this.sub$ = this.trigger$.subscribe((type: TriggerProgress) => {

        if(interval$) return;

        interval$ = setInterval(_ => {
          this.value < this.width ? this.value++ : this.value--;

          const isClear: boolean = this.value === this.width || this.value === 100 || this.value === 0;

          if(isClear) {
            clearInterval(interval$);
            interval$ = null;
          }

        }, 20);
      })
    })
  }

  
  trigger(type: TriggerProgress, step: number = 20) {
    switch(type) {
      case 'sub': {
        if(this.width > 0) {
          this.width -= step;
          this.trigger$.next(type);
        } else {
          this.width = 0;
        }

        break;
      }

      case 'add': {
        if(this.width < 100) {
          this.width += step;
          this.trigger$.next(type);
        } else {
          this.width = 100
        }

        break;
      }
    } 
  }
}
