import { Component,  Input, Output, EventEmitter } from '@angular/core';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

@Component({
  selector: 'ui-form-thumler-input',
  templateUrl: './form-thumler-input.component.html',
  styleUrls: ['./form-thumler-input.component.scss'],
})
export class FormThumlerInputComponent {

  @Input('option') option: TQuestionOption;
  @Input('disabled') disabled: boolean = false;
  @Input('isActive') isActive: boolean = false;

  @Output('setValue') _setValue: EventEmitter<TQuestionOption> = new EventEmitter();

  setValue():void {
    if(!this.disabled) {
      this._setValue.emit(this.option);
    }
  }
}
