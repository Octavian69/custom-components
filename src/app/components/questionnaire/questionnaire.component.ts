import { Component, OnInit } from '@angular/core';
import { QuestionnaireService } from 'src/app/services/questionnaire.service';
import { TQuestionLevel, QuestionType } from 'src/app/types/questionnaire.types';
import { Observable } from 'rxjs';
import { QuestionnaireFlags } from 'src/app/flags/questionnaire.flags';
import { QuestionnaireState } from 'src/app/states/questionnaire.state';
import { TChangeSide, TPosition } from 'src/app/types/Types';
import { ANVoidTranslate } from 'src/app/animations/animations';
import { delay, tap } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { IQuestionResult } from 'src/app/interfaces/questionnaire/IQuestionResult';

@Component({
  selector: 'ui-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss'],
  animations: [ANVoidTranslate(400)]
})
export class QuestionnaireComponent implements OnInit {

  public level$: Observable<TQuestionLevel> = this.questionService.getStream('level$');
  public formValue$: Observable<TQuestionLevel> = this.questionService.getStream('formValue$');
  public questions$: Observable<QuestionType[]> = this.questionService.getStream('questions$');
  public results$: Observable<IQuestionResult> = this.questionService.getStream<IQuestionResult>('results$');
  public question$: Observable<QuestionType> = this.getQuestionStream();
  public flags: QuestionnaireFlags = this.questionService.getFlagObject();
  public state: QuestionnaireState = this.questionService.getStateObject();

  constructor(
    private questionService: QuestionnaireService
  ) { }

  ngOnInit(): void {
  }

  getQuestionStream(): Observable<QuestionType> {
    return this.questionService
    .getStream<QuestionType>('question$')
    .pipe(
      delay(0),
      tap(_ => this.questionService.setFlag('isSetQuestion', false))
    );
  }

  getQuestionPosition(): TPosition {
    const question: QuestionType = this.questionService.getStreamValue('question$');
    const questions: QuestionType[] = this.questionService.getStreamValue('questions$');
    const isEmpty: boolean = !question || !questions;

    if(isEmpty) return null;
    
    const { length } = questions;
    const currentIdx: number = questions.indexOf(question);
    const isFirst: boolean = currentIdx  === 0;
    const isLast: boolean = currentIdx === (length - 1);

    switch(true) {
      case isFirst: return 'first';
      case isLast: return 'last';
      default: return null;
    }
  }

  isShowLoader(): boolean {
    return this.flags.isQuestionsRequest;
  }
  
  changeLevel(level: TQuestionLevel): void {
    this.questionService.changeLevel(level);
  }

  changeQuestion(side: TChangeSide): void {
    this.questionService.changeQuestion(side);
  }

  start(): void {
    this.questionService.start();
  }

  restart(): void {
    this.questionService.restart();
  }

  submit(form: FormGroup): void {
    this.questionService.submit(form);
  }

  calculate(): void {
    this.questionService.calculate();
  }
}
