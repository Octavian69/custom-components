import { Component, OnInit, Input, ViewChild, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { Theme, Simple, TaskListType } from 'src/app/types/Types';
import { CdkDragDrop, moveItemInArray, CdkDropList, transferArrayItem } from '@angular/cdk/drag-drop';
import { DragDropService } from 'src/app/services/drag-drop.service';
import { Observable, Subscription, timer } from 'rxjs';
@Component({
  selector: 'ui-drop-list',
  templateUrl: './drop-list.component.html',
  styleUrls: ['./drop-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class DropListComponent implements OnInit, AfterViewInit {
  
  tasks$: Observable<string[]>;
  connectedList: CdkDropList[];

  @Input('type') type: TaskListType;
  @Input('title') title: string;
  @Input('theme') theme: Theme;
  @ViewChild('todoList') todoList: CdkDropList;

  constructor(
    private dragDropService: DragDropService
  ) { }

  ngOnInit() {  
    this.init();
  }

  ngAfterViewInit() {
    this.initDropLists();
  }

  init(): void {
    this.initStream();
  }

  initStream(): void {
    this.tasks$ = this.dragDropService.getStream(this.type);
  }

  initDropLists(): void {
    this.dragDropService.registerDropList(this.todoList, this.type);

    const sub$: Subscription = timer(0).subscribe(_ => {
      this.connectedList = this.dragDropService.getConnectedCdkLists(this.type);
      sub$.unsubscribe();
    })
  }

  getListTheme(): Simple<boolean> {
    return {
      [this.theme]: true
    }
  }

  getItemTheme(): Simple<boolean> {
    const key: string = `${this.theme}-item`;
    return { [key]: true }
  }

  getTitleTheme(): Simple<boolean> {
    const key: string = `${this.theme}-title`;
    return {
      [key]: true
    }
  }
  
  drop(event: CdkDragDrop<string[]>): void {
    const { previousIndex, currentIndex, previousContainer, container } = event;

    if(previousContainer === container) {
      moveItemInArray(container.data, previousIndex, currentIndex);
    } else {
      transferArrayItem(
        previousContainer.data,
        container.data,
        previousIndex,
        currentIndex
      )

      this.dragDropService.replaceValue(previousContainer.data, container.data, currentIndex)

    }
  }
}
