import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';
import { TranslateService } from 'src/app/services/translate.service';
import { TranslateFlags } from 'src/app/flags/translate.flags';
import { LanguageOption, TranslateForm, TranslateFormKey } from 'src/app/types/translate.types';
import { DisabledStateMatcher } from 'src/app/models/forms/DisabledStateMatcher';
import { translateControlKeys } from 'src/app/db/translate.db';
import { ANTranslate } from 'src/app/animations/animations';

@UntilDestroy()
@Component({
  selector: 'ui-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss'],
  animations: [ANTranslate('-100%', '0%', 300, 'Y')]
})
export class TranslateComponent implements OnInit, OnDestroy {

  languages$: Observable<LanguageOption[]> = this.viewService.getStream('languages$');
  translate$: Observable<string> = this.viewService.getStream('translate$');

  form: FormGroup;
  previousValue: TranslateForm;
  stateMatcher: DisabledStateMatcher = new DisabledStateMatcher();
  flags: TranslateFlags = this.viewService.getFlagObject();

  @ViewChild('fromLangRef', { static: false }) fromLangRef: ElementRef;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private viewService: TranslateService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.viewService.unsubscribe();
  }

  init(): void {
    this.initForm();
    this.initTranslateSub();
    this.viewService.initTranslateSub();
    this.viewService.initLanguages();
  }

  initForm(): void {
    this.form = this.fb.group({
      fromLang: [null, [Validators.required, Validators.maxLength(500)]],
      toLang: [null, Validators.required],
      text: [null, Validators.required],
      translate: [null]
    });
  }

  initTranslateSub(): void {
    this.translate$
    .pipe(
      distinctUntilChanged(),
      untilDestroyed(this)
    )
    .subscribe((value: string) => {
      this.form.get('translate').setValue(value);
    })
  }

  getDisabledReplaceBtn(): boolean {
      const { value: { fromLang, toLang } } = this.form;
      const isDisabled: boolean = !(fromLang && toLang);

      return isDisabled;
  }

  getDisabledResetBtn(): boolean {
    const isEmptyForm: boolean = translateControlKeys.every((k: TranslateFormKey) => {
      return !this.form.get(k).value;
    });

    return isEmptyForm || this.viewService.getFlag('isTranslateRequest');
  }

  getTextFocusStatus(): boolean {
    if(this.fromLangRef) {
      const { nativeElement } = this.fromLangRef;
  
      return nativeElement && this.document.activeElement === nativeElement;
    }

    return false;
  }

  replace(): void {
    const { fromLang, toLang, translate } = this.form.getRawValue();
    const textValue: string = typeof translate === 'string' ? translate.substr(0, 500) : '';

    this.form.disable();
    this.form.get('fromLang').setValue(toLang);
    this.form.get('toLang').setValue(fromLang);
    this.form.get('text').setValue(textValue);
    this.form.get('translate').reset();
    this.form.enable();

    this.translate();
  }

  translate(): void {
    const { valid, disabled } = this.form;

    if(valid && !disabled) {
      const value: TranslateForm = this.form.getRawValue();
      this.previousValue = null;
      this.viewService.emitEvent<TranslateForm>('requestTranslate$', value)
    }
  }

  reset(): void {
    this.form.disable();
    this.previousValue = this.form.getRawValue();
    this.form.reset();
    this.form.enable();
  }

  returnPreviousValue(): void {
    this.form.disable();
    this.form.patchValue(this.previousValue);
    this.previousValue = null;
    this.form.enable();
  }

  copy(): void {
    const { translate } = this.form.getRawValue();
    
    window.navigator.clipboard.writeText(translate);
  }
}
