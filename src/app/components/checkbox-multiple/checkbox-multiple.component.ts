import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Role } from 'src/app/models/Role';
import { RoleType } from 'src/app/types/Types';
import { ANToogleSize, ANScale } from 'src/app/animations/animations';

@Component({
  selector: 'ui-checkbox-multiple',
  templateUrl: './checkbox-multiple.component.html',
  styleUrls: ['./checkbox-multiple.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ANToogleSize("height"), ANScale()]
})
export class CheckboxMultipleComponent {

  showState: 'open' | 'close' = 'open';

  @Input('roles') roles: Role[] = [];
  @Input('title') title: string;
  @Output('remove') _remove = new EventEmitter<Role>();

  remove(role: Role) {
    this._remove.emit(role);
  }

  change(e: boolean, role: Role) {
  }

  trackByFn(idx: number, role: Role): RoleType {
    return role.type;
  }

  toggleShowState(): void {
    this.showState = this.showState === 'close' ? 'open' : 'close'
  }
}
