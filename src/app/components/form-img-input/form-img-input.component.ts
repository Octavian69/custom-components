import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { TQuestionImgOption } from 'src/app/types/questionnaire.types';
import { fade } from 'src/app/animations/animations';

@Component({
  selector: 'ui-form-img-input',
  templateUrl: './form-img-input.component.html',
  styleUrls: ['./form-img-input.component.scss'],
  animations: [fade]
})
export class FormImgInputComponent implements OnInit {
  @Input('option') option: TQuestionImgOption;
  @Input('isActive') isActive: boolean = false;
  @Input('disabled') disabled: boolean = false;

  @Output('setValue') _setValue: EventEmitter<TQuestionImgOption> = new EventEmitter();

  ngOnInit() {
    // console.log(this.opt)
  }

  setValue():void {
    const isCanEmit: boolean = !this.disabled && !this.isActive;

    if(isCanEmit) {
      this._setValue.emit(this.option)
    }
  }
}
