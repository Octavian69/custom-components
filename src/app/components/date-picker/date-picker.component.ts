import { Component, OnInit, Provider, forwardRef, Injector, HostListener, ViewChild, ElementRef, Input } from '@angular/core';

import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgControl } from '@angular/forms';
import { DPStateManager } from 'src/app/models/datepicker-models/DPStateManager';
import { TPManager } from 'src/app/models/datepicker-models/TPManager';
import { DatePickerOption } from 'src/app/models/datepicker-models/DatepcikerOption';
import { TimeEvent, DateLimitType, PickerType } from 'src/app/types/datepicker.types';
import { copyDate } from 'src/app/handlers/datepicker.handlers';

const CONTROL: Provider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DatePickerComponent),
    multi: true
}

@Component({
  selector: 'ui-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [CONTROL]
})
export class DatePickerComponent implements OnInit, ControlValueAccessor {
  
  inputValue: Date;
  time: [string, string];
  dpStateManager: DPStateManager;
  timepickerManager: TPManager;
  minDate: Date = null;
  maxDate: Date = null;
  formControl: NgControl;

  @Input('minDate') set _minDate(date: Date) {
    this.minDate = date;

    if(this.dpStateManager) {
      this.checkedLimitDate('min', date);
    }  
  }
  @Input('maxDate') set _maxDate(date: Date) {
    this.maxDate = date;

    if(this.dpStateManager) {
      this.checkedLimitDate('max', date);
    } 
  }
  @ViewChild('mainRef') mainRef: ElementRef;
  @HostListener('window:click', ['$event'])
    checkedShowState(e: MouseEvent): void {
      const { nativeElement: datepicker$ } = this.mainRef;
      const isDateShow: boolean = this.dpStateManager.getShowState();
      const isTimeShow: boolean = this.timepickerManager.getShowState();
      const isPickerContain: boolean = (<any>e).path.includes(datepicker$);
      const isDateClose: boolean = isDateShow && !isPickerContain;
      const isTimeClose: boolean = isTimeShow && !isPickerContain
  
      if(isDateClose) this.dpStateManager.close();
      if(isTimeClose) this.timepickerManager.close()
    }
  

  onChange: (date: Date) => void = () => null;
  onTouch: () => void = () => null;

  constructor(
    private injector: Injector
  ) { }
  
  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.formControl = this.injector.get(NgControl);
    this.dpStateManager = new DPStateManager(new Date(), this.minDate, this.maxDate);
    this.timepickerManager = new TPManager(this.dpStateManager);
  }

  changeDate(option: DatePickerOption): void {
    this.dpStateManager.set(option);
    this.update();
  }

  changeTime(event: TimeEvent) {
    this.timepickerManager.set(event);
    this.update();
  }

  update(): void {
    const date: Date = this.dpStateManager.get();
    this.onChange(date);
    this.inputValue = copyDate(date);
    this.time = this.timepickerManager.get();
  }

  checkedLimitDate(type: DateLimitType, limitDate: Date): void {
    const currentDate: Date = this.dpStateManager.get();
    const value = this.formControl.value;

    this.dpStateManager.setLimitDate(type, limitDate);
    this.dpStateManager.setFullDate(currentDate);

    if(value) {
      this.update();
    } 
  }

  writeValue(date: Date): void {
    if(date) {
      this.dpStateManager.setFullDate(date);
      this.update();
    } else {
      this.reset();
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.dpStateManager.disabled = isDisabled;
  }

  show(type: PickerType): void {
    const isShowDate: boolean = this.dpStateManager.getShowState();
    const isShowTime: boolean = this.timepickerManager.getShowState();

    switch(type) {
      case 'datepicker': {
        if(isShowTime && !isShowDate) {
          this.timepickerManager.close();
        }

        this.dpStateManager.setShowState();

        break;
      }
      case 'timepicker': {
        if(isShowDate && !isShowTime) {
          this.dpStateManager.close();
        }

        this.timepickerManager.setShowState();

        break;
      }
    }
  }

  copy(value: string): void {
    window.navigator.clipboard.writeText(value).then();
  }

  cancel(): void {
    this.onChange(null);
    this.reset();
  }

  reset(): void {
    this.dpStateManager.setFullDate(null);
    this.inputValue = null;
    this.time = ['00', '00'];
    this.dpStateManager.close();
  }
}
