import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { QuestionType } from 'src/app/types/questionnaire.types';
import { TChangeSide, TPosition, TSide, Simple } from 'src/app/types/Types';
import { ANVoidTranslate } from 'src/app/animations/animations';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { FormValidator } from 'src/app/models/forms/FormValidator';
import { IQuestionResult } from 'src/app/interfaces/questionnaire/IQuestionResult';

@Component({
  selector: 'ui-questionnaire-form',
  templateUrl: './questionnaire-form.component.html',
  styleUrls: ['./questionnaire-form.component.scss'],
  animations: [ANVoidTranslate(600)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionnaireFormComponent implements OnInit {

  form: FormGroup;

  @Input('question') question: QuestionType;
  @Input('questions') questions: QuestionType[];
  @Input('position') position: TPosition;
  @Input('isSetStatus') isSetStatus: boolean;
  @Input('side') side: Exclude<TSide, 'top' | 'bottom'>;
  @Input('formValue') formValue: any;

  @Output('initialize') _initialize: EventEmitter<void> = new EventEmitter();
  @Output('changeQuestion') _changeQuestion: EventEmitter<TChangeSide> = new EventEmitter();
  @Output('submit') _submit: EventEmitter<FormGroup> = new EventEmitter(); 

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.complete();

    if(this.formValue) this.form.patchValue(this.formValue);
  }

  complete(): FormGroup {
    const form = this.questions.reduce((accum, q: QuestionType) => {
      const { formControlName, defaultValue, correctValue, multiple } = JSON.parse(JSON.stringify(q));

      accum[formControlName] = [
        defaultValue, 
        FormValidator.compose(correctValue, multiple), 
        []
      ];

      return accum
    }, {} as Simple<Array<any>>);

    return this.fb.group(form);
  }

  changeQuestion(side: TChangeSide): void {
    this._changeQuestion.emit(side);
  }

  submit(): void {
    this._submit.emit(this.form);
  }
}
