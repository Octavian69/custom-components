import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-layer-card',
  templateUrl: './layer-card.component.html',
  styleUrls: ['./layer-card.component.scss']
})
export class LayerCardComponent implements OnInit {

  cardText: string = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta, temporibus.';
  cards: number[] = new Array(5).fill(1);

  constructor() { }

  ngOnInit(): void {
  }

}
