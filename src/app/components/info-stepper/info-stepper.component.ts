import { Component, OnInit, NgZone, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { Step } from 'src/app/types/Types';
import { InfoStepperService } from 'src/app/services/info-stepper.service';

@Component({
  selector: 'ui-info-stepper',
  templateUrl: './info-stepper.component.html',
  styleUrls: ['./info-stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoStepperComponent implements OnInit {
  steps$: Observable<Step[]> = this.stepperService.getStream('steps$');

  constructor(
    private stepperService: InfoStepperService
  ) { }
  
  ngOnInit() {}
}
