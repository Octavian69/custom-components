import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Simple } from 'src/app/types/Types';

@Component({
  selector: 'ui-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss']
})
export class RangeComponent {

  isActive: boolean = false;
  offset: number = 0;
  
  @ViewChild('slideRef') slideRef: ElementRef;

  @HostListener("window:mouseup", ['$event'])
  setActive(e: Event) {
    this.isActive = false;
  }  

  changeRange(e: any): void {
    const { currentTarget: { offsetLeft, clientWidth }, clientX } = e;
    const offset: number = clientX - offsetLeft;

    if(this.isActive) {
      switch(true)  {
        case offset <= 0: {
          this.offset = 0;
          break;
        }
  
        case offset >= clientWidth: {
          this.offset = clientWidth;
          break;
        }
  
        default: {
          this.offset = offset;
        }
      }
    }
  }

  setActiveState(flag: boolean, e?: MouseEvent): void {
    this.isActive = flag;
  }

  setOffset(e: any): void {
    this.offset = e.clientX - e.currentTarget.offsetLeft;
  }

  getProgressStyle(): Simple<string> {
    return {
      background: `linear-gradient(to right,rgb(19, 177, 19) ${this.offset}px, antiquewhite ${this.offset}px)`
    }
  }

  preventDrag(e: Event) {
    return false;
  }

  getValue(progressRef: HTMLDivElement): string {
    const { clientWidth } = progressRef;

    const value: number = this.offset * 100 / clientWidth;

    return  value > 100 ? '100' : value < 0 ? '0' : value.toFixed(0);
  }

}
