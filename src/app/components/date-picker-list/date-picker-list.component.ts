import { Component, OnInit, Output, Input, EventEmitter, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { ANTranslate, ANScaleState, fade } from 'src/app/animations/animations';
import { TSide, Simple } from 'src/app/types/Types';
import { dpSectionTypes, dpDaysTitles } from 'src/app/db/datepicker.db';
import { DatePickerLevelType, DPSectionType, DayTitle, DPListType, DPAction } from 'src/app/types/datepicker.types';
import { DatePickerOption } from 'src/app/models/datepicker-models/DatepcikerOption';

@Component({
  selector: 'ui-date-picker-list',
  templateUrl: './date-picker-list.component.html',
  styleUrls: ['./date-picker-list.component.scss'],
  animations: [ANTranslate('-100%', '0px', 200, "Y"), ANScaleState(300, 0.6), fade],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerListComponent implements OnInit {
  
  type: DatePickerLevelType = 'year';
  currentTypeIdx: number = 0;
  sectionShowSide: TSide = 'left';
  isSetYearsOptions: boolean = false;
  
  sectionTypesList: Readonly<DPSectionType[]> = dpSectionTypes;
  daysTitles: Readonly<DayTitle[]> = this.modificatedDayTitles();
  

  @HostBinding('@fade') fade: any;
  @Input('type') set _type(type: DatePickerLevelType) {
    if(type) {
      const currentIdx: number = this.sectionTypesList.findIndex(opt => opt.type === type);
      this.sectionShowSide = currentIdx > this.currentTypeIdx ? 'right' : 'left';
      this.currentTypeIdx = currentIdx;
      this.type = type;
    }
  } 
  @Input('options') options: DPListType = [];
  @Input('isShowTimepicker') isShowTimepicker: boolean = true;
  @Input('date') date: Date;

  @Output('setValue') _setValue = new EventEmitter<DatePickerOption>();
  @Output('level') _level = new EventEmitter<DatePickerLevelType>();
  @Output('changeList') _changeList = new EventEmitter<DPAction>();

  constructor() { }

  ngOnInit(): void {}
  
  level(type: DatePickerLevelType): void {
    if(this.date) this._level.emit(type);
  }

  disabledChangeYear(side: DPAction): boolean {

    return false;
  }

  getDisabledPick(type: DatePickerLevelType): boolean {
    if(this.date) return false;

    return !this.date && type === 'year' ? false : true;
  }

  changeYearList(side: DPAction): void {
    this._changeList.emit(side);
  }

  getShowClass(): Simple<boolean> {
    return {
      'left-show': this.sectionShowSide === 'left',
      'right-show': this.sectionShowSide === 'right'
    }
  }

  modificatedDayTitles(): Readonly<DayTitle[]> {
    const dayTitles: DayTitle[] = dpDaysTitles.concat();
    const sunday: DayTitle = dayTitles.shift();

    dayTitles.push(sunday);

    return dayTitles;
  }

  trackByFn(idx: number, option: DatePickerOption) {
    return idx;
  }

  setValue(option: DatePickerOption): void { 
    this._setValue.emit(option);
  }

  setDateValue(option: DatePickerOption): void {
    const { current, disabled } = option;
    const isCanEmit: boolean = !disabled && !current;

    if(isCanEmit) this.setValue(option);
  }
}
