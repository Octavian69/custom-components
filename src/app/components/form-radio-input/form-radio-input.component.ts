import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TQuestionOption } from 'src/app/types/questionnaire.types';

@Component({
  selector: 'ui-form-radio-input',
  templateUrl: './form-radio-input.component.html',
  styleUrls: ['./form-radio-input.component.scss']
})
export class FormRadioInputComponent {
  
  @Input('option') option: TQuestionOption;
  @Input('isActive') isActive: boolean = false;
  @Input('disabled') disabled: boolean = false;

  @Output('setValue') _setValue: EventEmitter<TQuestionOption> = new EventEmitter();

  setValue(): void {
    const isCanEmit: boolean = !this.disabled && !this.isActive;

    if(isCanEmit) {
      this._setValue.emit(this.option);
    }
  }
  
}
