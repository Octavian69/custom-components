import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ui-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss']
})
export class CircleComponent implements OnInit {
  
  percent: number = 0;
  percentStep: string = '0'
  interval$: any;
  R: number = 50;
  D: number = this.R * 2;
  L: number = this.D * 3.14;

  @Input('strokeColor') strokeColor: string = 'rgba(67, 200, 241, 0.8)';
  @Input('strokeWidth') strokeWidth: string = '10';
  @Input('textSize') textSize: string = '20px';
  @Input('textColor') textColor: string = 'lightcoral'

  @Output('action') _action = new EventEmitter<void>();

  ngOnInit() {
    const step: number = this.L / 10;

    this.interval$ = setInterval(_ => {
      if(this.percent >= this.L) {
        this.percent = this.L;
        
        this.clear();
      } else {
        this.percent += step;
      }

      this.percentStep = (this.percent * 100 / this.L).toFixed(0);
    }, 150)
  }

  clear(): void {
    clearInterval(this.interval$);
    this.interval$ = null;
    this.emit();
  }

  emit(): void {
    this._action.emit();
  }

}
