import { Component, Output, EventEmitter, ChangeDetectionStrategy, Input } from '@angular/core';
import { TQuestionLevel, TQuestionResultState, TQuestionResultType, TQuestionResultOption } from 'src/app/types/questionnaire.types';
import { fade, ANVoidTranslate } from 'src/app/animations/animations';
import { IQuestionResult } from 'src/app/interfaces/questionnaire/IQuestionResult';
import { Simple } from 'src/app/types/Types';

@Component({
  selector: 'ui-questionnaire-result',
  templateUrl: './questionnaire-result.component.html',
  styleUrls: ['./questionnaire-result.component.scss'],
  animations: [fade, ANVoidTranslate(400)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionnaireResultComponent {

  @Input('state') state: TQuestionResultState;
  @Input('results') results: IQuestionResult;

  @Output('return') _return: EventEmitter<TQuestionLevel> = new EventEmitter();
  @Output('restart') _restart: EventEmitter<void> = new EventEmitter();
  @Output('calculate') _calculate: EventEmitter<void> = new EventEmitter();
  
  getListClass(key: TQuestionResultType, options: TQuestionResultOption[]): Simple<boolean> {
    
    if(options.length) {
      return {
        [key]: true
      }
    }

    return {
      hide: true
    }
  }

  getResultParts(): Partial<IQuestionResult> {
    const { failed, success } = this.results;

    return { failed, success };
  }

  getEfficiencyClass(): Simple<boolean> {
    if(this.results) {

      const { efficiency } = this.results;

      return {
        low: efficiency < 30,
        middle: efficiency >= 30 && efficiency < 70,
        high: efficiency >= 70
      };

    }

    return {};
  }

  return(): void {
     this._return.emit('form');   
  }

  calculate(): void {
    this._calculate.emit();
  }

  restart(): void {
    this._restart.emit(); 
  }
}
