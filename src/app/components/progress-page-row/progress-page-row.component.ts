import { Component, OnInit, HostListener } from '@angular/core';
import { fade } from 'src/app/animations/animations';

@Component({
  selector: 'ui-progress-page-row',
  templateUrl: './progress-page-row.component.html',
  styleUrls: ['./progress-page-row.component.scss'],
  animations: [fade]
})
export class ProgressPageRowComponent implements OnInit {

  width: number = 0;

  @HostListener('window:scroll', ['$event'])
    scroll(e: MouseEvent) {
      const { scrollHeight, scrollTop, clientHeight } = document.documentElement;
      const endBreakPoint: number = scrollHeight - clientHeight;
      const offset: number = (scrollTop * 100) / endBreakPoint;

      this.width = offset;
    }

  constructor() { }

  ngOnInit(): void {
  }

}
