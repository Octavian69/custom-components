import { Component, Input, OnInit, Injector, Provider } from '@angular/core';
import { TQuestionImgOption } from 'src/app/types/questionnaire.types';
import { NgControl, ControlValueAccessor } from '@angular/forms';
import { ControlProvider } from 'src/app/models/forms/ControlProvider';

@Component({
  selector: 'ui-form-img',
  templateUrl: './form-img.component.html',
  styleUrls: ['./form-img.component.scss'],
  providers: [new ControlProvider(FormImgComponent) as Provider]
})
export class FormImgComponent implements OnInit, ControlValueAccessor {
  
  value: any = null;
  disabled: boolean = false;
  formControl: NgControl;

  onChange: (v: any) => void = (v: any) => {};
  onTouch: () => void = () => {};

  @Input('options') options: TQuestionImgOption[] = [];

  constructor(
    private injector: Injector
  ) {}

  ngOnInit() {
    this.init();
  }

  init(): void {
    this.formControl = this.injector.get(NgControl);
  }

  setValue(opt: TQuestionImgOption): void {
    const { value } = opt;
    this.value = value;
    this.onChange(this.value);
    this.onTouch();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
  }
}
