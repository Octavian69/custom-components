import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-input-range',
  templateUrl: './input-range.component.html',
  styleUrls: ['./input-range.component.scss']
})
export class InputRangeComponent {

  rangeValue: number = 20;
  step: number = 5;
  min: number = 20;
  max: number = 60;

  @Input('toggle') toggle: number;
  @Input('toggle2') toggle2: number;

  getValue(): string {
    const value: string = this.rangeValue + '000';

    return value;
  }

  getProgressStyle() {

    const difference: number = this.max - this.min;
     
    const value: number = (this.rangeValue - this.min) * 100 / difference;

    return {
      background: `linear-gradient(90deg, lightgreen 0%, lightgreen ${value}%, white ${value}%, white 100%)`
    }

  }
}
