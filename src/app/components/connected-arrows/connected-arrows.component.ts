import { Component, OnInit, Inject, ViewChildren, QueryList, AfterViewInit, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { LEADER_TOKEN } from 'src/app/injection/tokens';
import { ConnectedLineMethod } from 'src/app/types/Types';
import { timer } from 'rxjs';

@Component({
  selector: 'ui-connected-arrows',
  templateUrl: './connected-arrows.component.html',
  styleUrls: ['./connected-arrows.component.scss']
})
export class ConnectedArrowsComponent implements OnInit, AfterViewInit {

  settings: Map<string, ConnectedLineMethod> = new Map([
    ['block_1', 'firstLine'],
    ['block_2', 'secondLine'],
    ['block_3', 'thirdLine'],
    ['block_4', 'fourthLine']
  ])

  @ViewChildren('blockRef') blockRefs: QueryList<HTMLDivElement>

  constructor(
    @Inject(LEADER_TOKEN) private LeaderLine: any
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit() {
    timer(100).subscribe(_ => this.init());
  }

  init() {
    const refs: ElementRef[] = this.blockRefs.toArray() as any;
    const { nativeElement: connected$ } = refs
    .find((block$: ElementRef<HTMLDivElement>) => block$.nativeElement.classList.contains('block_connected'));

    const elems$: HTMLDivElement[] = refs
    .filter((block$: ElementRef<HTMLDivElement>) => !block$.nativeElement.classList.contains('block_connected'))
    .map(({ nativeElement }) => nativeElement);
  

    elems$.forEach((block$: HTMLDivElement) => {
      const { type } = block$.dataset;
      const method: ConnectedLineMethod = this.settings.get(type);

      this[method](block$, connected$);
    })
  };
  
  firstLine(block$: HTMLDivElement, connected$: HTMLDivElement): void {

    const { LeaderLine } = this;
    const line = new LeaderLine(
      block$,
      LeaderLine.pointAnchor(connected$, {
        x: 0,
        y: 0
      }),
      {
        middleLabel: LeaderLine.pathLabel('First', {
          color: 'rgb(183, 152, 145)'
        }),
        animOptions: {duration: 5500, timing: [0.58, 0, 0.42, 1]},
        showEffectName: 'draw',
        letterSpacing: '20px',
        startSocket: 'right',
        startPlug: 'disc',
        dropShadow: true,
        gradient: true,
        startPlugColor: 'rgb(148, 113, 107)',
        endPlugColor: 'rgb(249, 83, 198)',
        color: 'rgb(183, 152, 145)'
      });
  }

  secondLine(block$: HTMLDivElement, connected$: HTMLDivElement): void {
    const { LeaderLine } = this;
    const line = new LeaderLine(block$,      
      LeaderLine.pointAnchor(connected$, {
        x: '100%',
        y: 0
      }),
    {
      startLabel: LeaderLine.captionLabel('Second', {
        color: 'rgb(0, 131, 176)',
        fontFamily: 'Krona One',
        letterSpacing: '2px',
        fontSize: '.8rem'
      }),
      startPlug: 'arrow3',
      dash: {animation: true},
      startPlugColor: 'rgb(0, 180, 219)',
      endPlugColor: 'rgb(185, 29, 115)',
      gradient: true
    });
  }

  thirdLine(block$: HTMLDivElement, connected$: HTMLDivElement): void {
    const { LeaderLine } = this;
    const background: string = 'linear-gradient(to right, rgb(241, 39, 17), rgb(245, 175, 25))';
    const line = new LeaderLine(
      LeaderLine.mouseHoverAnchor({
        element: block$,
        showEffectName: 'draw',
        style: {
          background,
          cursor: 'pointer',
          textAlign: 'center',
          transition: 'transform .3s linear'
        },
        hoverStyle: {
          background,
          transform: 'scale(1.1)'
        }
      }),
      LeaderLine.pointAnchor(connected$, {
        x: '100%',
        y: '50%'
      }),
      {
        startLabel: LeaderLine.pathLabel('Third', {
          color: 'rgb(241, 39, 17)'
        }),
        letterSpacing: '20px',
        endSocket: 'right',
        startPlug: 'disc',
        dropShadow: true,
        gradient: true,
        startPlugColor: 'rgb(245, 175, 25)',
        endPlugColor: 'rgb(185, 29, 115)',
        color: 'rgb(183, 152, 145)',
        outline: true,
        outlineColor: '#FABE0E'
      });
  }

  fourthLine(block$: HTMLDivElement, connected$: HTMLDivElement): void {
    const { LeaderLine } = this;
    const line = new LeaderLine(
      block$,
      LeaderLine.pointAnchor(connected$, {
        x: '0%',
        y: '50%'
      }),
      {
        startLabel: LeaderLine.captionLabel('Fourth', {
          color: 'rgb(196, 113, 237)'
        }),
        letterSpacing: '20px',
        startScoket: 'bottom',
        endSocket: 'left',
        endPlug: 'arrow2',
        dropShadow: {
          dx: 6,
          dy: 8,
          blur: 0.2
        },
        gradient: true,
        startPlugColor: 'rgb(18, 194, 233)',
        endPlugColor: 'rgb(249, 83, 198)'
      });
  }
}
