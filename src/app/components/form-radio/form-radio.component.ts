import { Component, OnInit, Input, Injector, Provider } from '@angular/core';
import { TQuestionOption } from 'src/app/types/questionnaire.types';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { ControlProvider } from 'src/app/models/forms/ControlProvider';


@Component({
  selector: 'ui-form-radio',
  templateUrl: './form-radio.component.html',
  styleUrls: ['./form-radio.component.scss'],
  providers: [ new ControlProvider(FormRadioComponent) as Provider ]
})
export class FormRadioComponent implements OnInit, ControlValueAccessor {

  value: any = null;
  disabled: boolean = false;
  formControl: NgControl;

  onChange: (v: any) => void = _ => {};
  onTouch: () => void = () => {};
  
  @Input('options') options: TQuestionOption[] = [];

  constructor(
    private injector: Injector
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.formControl = this.injector.get(NgControl);
  }

  setValue(option: TQuestionOption) {
    const { value } = option;
    this.onChange(value);
    this.onTouch();
    this.value = value;
  }

  registerOnChange(fn): void {
    this.onChange = fn;
  }

  registerOnTouched(fn): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
  }
}
