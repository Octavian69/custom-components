import { Component } from '@angular/core'

@Component({
  selector: 'ui-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss']
})
export class PhoneComponent {}
