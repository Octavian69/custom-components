import { Component, OnInit, HostBinding, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { fade } from 'src/app/animations/animations';
import { TimeEvent, TimeType, DPAction } from 'src/app/types/datepicker.types';

@Component({
  selector: 'ui-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],
  animations: [fade],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimepickerComponent {

  @HostBinding('@fade') fade: any;
  @Input('time') time: [string, string]; // [часы, минуты]
  @Output('change') _change = new EventEmitter<TimeEvent>();

  change(type: TimeType, action: DPAction): void {
      this._change.emit({ type, action });
  }

  wheel(e: WheelEvent, type: TimeType): void {
    e.preventDefault();

    const delta = Math.sign(e.deltaY);
    const action: DPAction = delta < 0 ? 'inc' : 'dec';

    this.change(type, action);
  }

}

