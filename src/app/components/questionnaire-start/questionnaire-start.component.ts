import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { TQuestionLevel } from 'src/app/types/questionnaire.types';

@Component({
  selector: 'ui-questionnaire-start',
  templateUrl: './questionnaire-start.component.html',
  styleUrls: ['./questionnaire-start.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuestionnaireStartComponent {
  
  @Output('start') _start: EventEmitter<TQuestionLevel> = new EventEmitter();

  start(): void {
    this._start.emit('form');
  }
}
