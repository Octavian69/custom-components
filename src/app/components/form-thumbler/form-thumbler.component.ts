import { Component, OnInit, Provider, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgControl } from '@angular/forms';
import { TQuestionOption } from 'src/app/types/questionnaire.types';
import { ControlProvider } from 'src/app/models/forms/ControlProvider';

@Component({
  selector: 'ui-form-thumbler',
  templateUrl: './form-thumbler.component.html',
  styleUrls: ['./form-thumbler.component.scss'],
  providers: [new ControlProvider(FormThumblerComponent) as Provider]
})
export class FormThumblerComponent implements OnInit, ControlValueAccessor {
  
  value: any[] = [];
  formControl: NgControl;
  disabled: boolean = false;

  onChange: (v: any) => void = (v: any) => {};
  onTouch: () => void = () => {};

  @Input('options') options: TQuestionOption[] = [];

  constructor(
    private injector: Injector
  ) { }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.formControl = this.injector.get(NgControl);
  }

  setValue(opt: TQuestionOption): void {
    const { value } = opt;
    const idx: number = this.value.indexOf(value);

    if(~idx) this.value.splice(idx, 1)
    else this.value.push(value);

    this.onChange(this.value)
    this.onTouch();
  }

  writeValue(value: any): void {
    if(value) this.value = value;
    else this.value = [];
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn
  }

  setDisabledState(flag: boolean): void {
    this.disabled = flag;
  }

  isActive(opt: TQuestionOption): boolean {
    const isActive: boolean = this.value.includes(opt.value);

    return isActive;
  }
}
