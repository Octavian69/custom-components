import { InjectionToken } from "@angular/core";
import { ServicesModule } from '../modules/services.module';

declare const LeaderLine: any;

export const LEADER_TOKEN: InjectionToken<any> = new InjectionToken<any>('LeaderLine', {
    providedIn: ServicesModule,
    factory: (): any => LeaderLine
});
