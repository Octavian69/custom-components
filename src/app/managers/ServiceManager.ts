import { Observable, BehaviorSubject, Subject } from 'rxjs';

export class ServiceManager<F, S> {
    unsubscriber$: Subject<boolean> = new Subject();

    constructor(
        public flags: F = null,
        public state: S = null
    ) {}

    getStream<T>(key: string): Observable<T> {
        return this[key].asObservable();
    }

    getStreamValue<T>(key: string): T {
        return (this[key] as BehaviorSubject<T>).getValue();
    }

    emitEvent<T>(streamKey: string, data: T): void {
        this[streamKey].next(data);
    }

    getFlag<K extends keyof F>(key: K): F[K] {
        return this.flags[key];
    }

    setFlag<K extends keyof F>(key: K, value: F[K]): void {
        this.flags[key] = value;
    }

    getFlagObject(): F {
        return this.flags;
    }

    resetFlagObject(flags: F): void {
        Object.assign(this.flags, flags);
    }

    getStateProp<K extends keyof S>(key: K): S[K] {
        return this.state[key];
    }

    setStateProp<K extends keyof S>(key: K, value: S[K]): void {
        this.state[key] = value;
    }

    getStateObject(): S {
        return this.state;
    }

    resetStateObject(state: S): void {
        Object.assign(this.state, state);
    }

    unsubscribe(): void {
        this.unsubscriber$.next(true);
    }
}