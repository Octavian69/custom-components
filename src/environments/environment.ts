// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  YA_TRANSLATE_URL: "https://translate.yandex.net/api/v1.5",
  API_YA_KEY: "trnsl.1.1.20200427T102602Z.9d1bd5ad412219e3.c1c971fd711cdb19c23bfb5becf1ff568e4e1fff",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
